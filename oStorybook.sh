#!/bin/sh

echo "Java VM version:"
java -version

echo "starting oStorybook ..."
java -Dfile.encoding=UTF-8 -splash:splash.png -jar /usr/share/ostorybook/oStorybook.jar $*
echo "done."

