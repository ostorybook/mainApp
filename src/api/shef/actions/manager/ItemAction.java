/*
 * Copyright (C) oStorybook Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Parts of this code are from the NovaWorx project. Other parts are 
 * from the SHEF project developed and published by Bob Tantlinger on
 * SourceForge.
 *
*/
package api.shef.actions.manager;

import java.awt.event.ItemListener;
import javax.swing.Action;

public interface ItemAction extends Action {

	void setSelected(boolean param);

	boolean isSelected();

	void addItemListener(ItemListener param);

	void removeItemListener(ItemListener param);
}
