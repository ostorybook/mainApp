/**
 * this package source is from https://sourceforge.net/projects/jortho/
 * version : 1.0 (11/03/2013)
 *
 * JOrtho is a spell checker for Java. The library works
 * with any JTextComponent from the swing framework. The
 * dictionary is based on the free Wiktionary.org and
 * applicable for multiple languages. You can select
 * the spellchecking language via context menu.
 *
 * GNU GENERAL PUBLIC LICENSE
 * Version 2, June 1991
 *
 * Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package api.jortho;

/**
 *
 * @author favdb
 */
public class About_package {

}
