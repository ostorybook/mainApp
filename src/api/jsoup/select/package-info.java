/**
 Packages to support the CSS-style element selector.
 {@link api.jsoup.select.Selector Selector defines the query syntax.}
 */
package api.jsoup.select;
