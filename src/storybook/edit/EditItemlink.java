/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.edit;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.JComboBox;
import storybook.Const;
import storybook.model.book.Book;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.DB.DATA;
import storybook.model.hbn.entity.Item;
import storybook.model.hbn.entity.Itemlink;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Person;
import storybook.model.hbn.entity.Scene;
import storybook.ui.Ui;

/**
 *
 * @author favdb
 */
public class EditItemlink extends AbstractEditor {

	private JComboBox cbSceneStart;
	private JComboBox cbSceneEnd;
	private JComboBox cbPerson;
	private JComboBox cbLocation;
	private JComboBox cbItem;

	public EditItemlink(Editor m, AbstractEntity e) {
		super(m, e, "110");
		initAll();
	}

	@Override
	public void initUpper() {
		Itemlink itemlink = (Itemlink) entity;
		cbItem = Ui.initAutoCombo(pUpper, mainFrame,
				DATA.ITEM, Book.TYPE.ITEM,
				null, null, "100");
		Scene d = itemlink.getStartScene();
		cbSceneStart = Ui.initAutoCombo(pUpper, mainFrame,
				DATA.SCENE_START, Book.TYPE.SCENE,
				d, null, "001");
		Scene f = ((Itemlink) entity).getEndScene();
		cbSceneEnd = Ui.initAutoCombo(pUpper, mainFrame,
				DATA.SCENE_END, Book.TYPE.SCENE,
				f, null, "001");
		cbPerson = Ui.initAutoCombo(pUpper, mainFrame,
				DATA.PERSON, Book.TYPE.PERSON,
				itemlink.getPerson(), null, "001");
		cbLocation = Ui.initAutoCombo(pUpper, mainFrame,
				DATA.LOCATION, Book.TYPE.LOCATION,
				itemlink.getLocation(), null, "001");
	}

	@Override
	public boolean verifier() {
		resetError();
		if (cbItem.getSelectedIndex() < 0) {
			errorMsg(cbItem, Const.ERROR_MISSING);
		}
		if (cbSceneStart.getSelectedIndex() != -1
				&& cbSceneEnd.getSelectedIndex() == -1) {
			errorMsg(cbSceneEnd, Const.ERROR_MISSING);
		}
		if (cbSceneStart.getSelectedIndex() == -1
				&& cbSceneEnd.getSelectedIndex() != -1) {
			errorMsg(cbSceneStart, Const.ERROR_MISSING);
		}
		if (cbPerson.getSelectedIndex() < 1
				&& cbLocation.getSelectedIndex() < 1) {
			errorMsg(cbPerson, Const.ERROR_MISSING);
			errorMsg(cbLocation, Const.ERROR_MISSING);
		}
		return (msgError.isEmpty());
	}

	@Override
	public void apply() {
		Itemlink itemlink = ((Itemlink) entity);
		itemlink.setItem((Item) cbItem.getSelectedItem());
		if (cbSceneStart.getSelectedIndex() > 0) {
			itemlink.setStartScene((Scene) cbSceneStart.getSelectedItem());
		} else {
			itemlink.setStartScene(null);
		}
		if (cbSceneEnd.getSelectedIndex() > 0) {
			itemlink.setEndScene((Scene) cbSceneEnd.getSelectedItem());
		} else {
			itemlink.setEndScene(null);
		}
		if (cbPerson.getSelectedIndex() > 0) {
			itemlink.setPerson((Person) cbPerson.getSelectedItem());
		} else {
			itemlink.setPerson();
		}
		if (cbLocation.getSelectedIndex() > 0) {
			itemlink.setLocation((Location) cbLocation.getSelectedItem());
		} else {
			itemlink.setLocation();
		}
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
