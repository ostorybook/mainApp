/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.edit;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.JComboBox;
import storybook.Const;
import storybook.model.book.Book;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.DB.DATA;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Person;
import storybook.model.hbn.entity.Scene;
import storybook.model.hbn.entity.Tag;
import storybook.model.hbn.entity.Taglink;
import storybook.ui.Ui;

/**
 *
 * @author favdb
 */
public class EditTaglink extends AbstractEditor {

	private JComboBox cbSceneStart;
	private JComboBox cbSceneEnd;
	private JComboBox cbPerson;
	private JComboBox cbLocation;
	private JComboBox cbTag;

	public EditTaglink(Editor m, AbstractEntity e) {
		super(m, e, "110");
		initAll();
	}

	@Override
	public void initUpper() {
		Taglink taglink = (Taglink) entity;
		cbTag = Ui.initAutoCombo(pUpper, mainFrame,
				DATA.TAG, Book.TYPE.TAG,
				null, null, "000");
		Scene d = taglink.getStartScene();
		cbSceneStart = Ui.initAutoCombo(pUpper, mainFrame,
				DATA.SCENE_START, Book.TYPE.SCENE,
				d, null, "001");
		Scene f = taglink.getEndScene();
		cbSceneEnd = Ui.initAutoCombo(pUpper, mainFrame,
				DATA.SCENE_END, Book.TYPE.SCENE,
				f, null, "001");
		cbPerson = Ui.initAutoCombo(pUpper, mainFrame,
				DATA.PERSON, Book.TYPE.PERSON,
				taglink.getPerson(), null, "001");
		cbLocation = Ui.initAutoCombo(pUpper, mainFrame,
				DATA.LOCATION, Book.TYPE.LOCATION,
				taglink.getLocation(), null, "001");
	}

	@Override
	public boolean verifier() {
		resetError();
		if (cbSceneStart.getSelectedIndex() != -1
				&& cbSceneEnd.getSelectedIndex() == -1) {
			errorMsg(cbSceneEnd, Const.ERROR_MISSING);
		}
		if (cbSceneStart.getSelectedIndex() == -1
				&& cbSceneEnd.getSelectedIndex() != -1) {
			errorMsg(cbSceneStart, Const.ERROR_MISSING);
		}
		if (cbPerson.getSelectedIndex() < 1
				&& cbLocation.getSelectedIndex() < 1) {
			errorMsg(cbPerson, Const.ERROR_MISSING);
			errorMsg(cbLocation, Const.ERROR_MISSING);
		}
		return (msgError.isEmpty());
	}

	@Override
	public void apply() {
		Taglink taglink = (Taglink) entity;
		taglink.setTag((Tag) cbTag.getSelectedItem());
		if (cbSceneStart.getSelectedIndex() < 1) {
			taglink.setStartScene(null);
		} else {
			taglink.setStartScene((Scene) cbSceneStart.getSelectedItem());
		}
		if (cbSceneEnd.getSelectedIndex() < 1) {
			taglink.setEndScene(null);
		} else {
			taglink.setEndScene((Scene) cbSceneEnd.getSelectedItem());
		}
		if (cbPerson.getSelectedIndex() < 1) {
			taglink.setPerson();
		} else {
			taglink.setPerson((Person) cbPerson.getSelectedItem());
		}
		if (cbLocation.getSelectedIndex() < 1) {
			taglink.setLocation();
		} else {
			taglink.setLocation((Location) cbLocation.getSelectedItem());
		}
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		// empty
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// empty
	}

}
