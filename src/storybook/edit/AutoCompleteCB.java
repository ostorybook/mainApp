/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.edit;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import api.mig.swing.MigLayout;
import resources.icons.ICONS;
import resources.icons.IconUtil;
import storybook.tools.swing.SwingUtil;
import storybook.ui.panel.AbstractPanel;

/**
 *
 * @author favdb
 */
public class AutoCompleteCB extends AbstractPanel {

	private JComboBox combo;
	private JButton btClear;
	private boolean setPreferredSize = true;
	private boolean addClearButton = true;
	private final String title;
	private final boolean isMandatory;
	private final boolean isNew;

	public AutoCompleteCB(String title, boolean isMandatory, boolean isNew) {
		super();
		this.title = title;
		this.isMandatory = isMandatory;
		this.setPreferredSize = true;
		this.addClearButton = true;
		this.isNew = isNew;
		initAll();
	}

	public AutoCompleteCB(String title, boolean isMandatory, boolean setPreferredSize,
			boolean addClearButton, boolean isNew) {
		super();
		this.title = title;
		this.isMandatory = isMandatory;
		this.setPreferredSize = setPreferredSize;
		this.addClearButton = addClearButton;
		this.isNew = isNew;
		initAll();
	}

	@Override
	public void init() {
		// empty
	}

	@Override
	public void initUi() {
		setName("cbpanel_" + title);
		setLayout(new MigLayout("ins 0"));

		combo = new JComboBox();
		combo.setName(title);
		combo.setEditable(true);
		if (setPreferredSize) {
			combo.setPreferredSize(new Dimension(250, 26));
		}
		//AutoCompleteDecorator.decorate(combo);
		add(combo);

		if (addClearButton) {
			btClear = new JButton(IconUtil.getIconSmall(ICONS.K.CLEAR));
			btClear.addActionListener(getClearAction());
			btClear.setBorderPainted(false);
			btClear.setOpaque(false);
			btClear.setContentAreaFilled(false);
			SwingUtil.setForcedSize(btClear, new Dimension(20, 20));
			add(btClear);
		}

	}

	public AbstractAction getClearAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				combo.setSelectedItem("");
			}
		};
	}

	public JComboBox getComboBox() {
		return (combo);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		// empty
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// empty
	}

}
