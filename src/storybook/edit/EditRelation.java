/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.edit;

import api.mig.swing.MigLayout;
import i18n.I18N;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import storybook.Const;
import storybook.model.book.Book;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.hbn.entity.Item;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Person;
import storybook.model.hbn.entity.Relationship;
import storybook.model.hbn.entity.Scene;
import storybook.tools.swing.js.JSCheckList;
import storybook.ui.MIG;
import storybook.ui.Ui;
import static storybook.ui.Ui.*;

/**
 *
 * @author favdb
 */
public class EditRelation extends AbstractEditor {

	private JComboBox cbSceneStart;
	private JComboBox cbSceneEnd;
	private JSCheckList lsPersons;
	private JSCheckList lsLocations;
	private JSCheckList lsItems;

	public EditRelation(Editor m, AbstractEntity e) {
		super(m, e, "110");
		initAll();
	}

	@Override
	public void initUpper() {
		Relationship relation = (Relationship) entity;
		pUpper.add(new JLabel(I18N.getColonMsg("scene.start")));
		Scene d = relation.getStartScene();
		cbSceneStart = Ui.getCB(pUpper, this, Book.TYPE.SCENE, d,
			null, BEMPTY);
		pUpper.add(new JLabel(I18N.getColonMsg("scene.end")));
		Scene f = relation.getEndScene();
		cbSceneEnd = Ui.getCB(pUpper, this, Book.TYPE.SCENE, f,
			null, BEMPTY);
		JPanel links = new JPanel(new MigLayout(MIG.FILLX));
		lsPersons = Ui.initCkList(links, mainFrame, Book.TYPE.PERSON,
			relation.getPersons(), null, BBORDER);
		if (relation.hasPerson1()) {
			lsPersons.selectEntity(relation.getPerson1());
		}
		if (relation.hasPerson2()) {
			lsPersons.selectEntity(relation.getPerson2());
		}
		lsLocations = Ui.initCkList(links, mainFrame, Book.TYPE.LOCATION,
			relation.getLocations(), null, BBORDER);
		lsItems = Ui.initCkList(links, mainFrame, Book.TYPE.ITEM,
			relation.getItems(), null, BBORDER);
		tab1.add(I18N.getMsg("links"), links);
	}

	@Override
	public boolean verifier() {
		resetError();
		if (cbSceneStart.getSelectedItem() != null
			&& cbSceneEnd.getSelectedItem() == null) {
			errorMsg(cbSceneEnd, Const.ERROR_MISSING);
		}
		if (lsPersons.getSelectedEntities().isEmpty()
			&& lsLocations.getSelectedEntities().isEmpty()
			&& lsItems.getSelectedEntities().isEmpty()) {
			errorMsg(lsPersons, "error.must_select");
		}
		return (msgError.isEmpty());
	}

	@Override
	public void apply() {
		Relationship relation = (Relationship) entity;
		if (cbSceneStart.getSelectedIndex() > 0) {
			relation.setStartScene((Scene) cbSceneStart.getSelectedItem());
		} else {
			relation.setStartScene(null);
		}
		if (cbSceneEnd.getSelectedIndex() > 0) {
			relation.setEndScene((Scene) cbSceneEnd.getSelectedItem());
		} else {
			relation.setEndScene(null);
		}
		relation.setPerson1();
		relation.setPerson2();
		List<AbstractEntity> ls;
		{
			ls = lsPersons.getSelectedEntities();
			List<Person> lsc = new ArrayList<>();
			for (AbstractEntity e : ls) {
				lsc.add((Person) e);
			}
			relation.setPersons(lsc);
		}
		{
			ls = lsLocations.getSelectedEntities();
			List<Location> lsc = new ArrayList<>();
			for (AbstractEntity e : ls) {
				lsc.add((Location) e);
			}
			relation.setLocations(lsc);
		}
		{
			ls = lsItems.getSelectedEntities();
			List<Item> lsc = new ArrayList<>();
			for (AbstractEntity e : ls) {
				lsc.add((Item) e);
			}
			relation.setItems(lsc);
		}
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		// empty
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// empty
	}

}
