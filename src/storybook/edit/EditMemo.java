/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.edit;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import storybook.Const;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.tools.html.Html;

/**
 *
 * @author favdb
 */
public class EditMemo extends AbstractEditor {

	public EditMemo(Editor m, AbstractEntity e) {
		super(m, e, "010");
		setName("PanelMemo");
		initAll();
	}

	@Override
	public void initUpper() {
		btDeploy.setVisible(false);
	}

	@Override
	public boolean verifier() {
		resetError();
		if (Html.htmlToText(hNotes.getText()).isEmpty()) {
			errorMsg(hNotes, Const.ERROR_MISSING);
		}
		return (msgError.isEmpty());
	}

	@Override
	public void apply() {
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		// empty
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// empty
	}

}
