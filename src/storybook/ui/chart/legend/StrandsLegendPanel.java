/*
 * SbApp: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package storybook.ui.chart.legend;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import org.hibernate.Session;
import i18n.I18N;
import storybook.model.Model;
import storybook.model.hbn.dao.StrandDAO;
import storybook.model.hbn.entity.Strand;
import storybook.tools.swing.ColorUtil;
import storybook.tools.swing.js.JSLabel;
import storybook.ui.MainFrame;

public class StrandsLegendPanel extends AbstractLegendPanel {

    public StrandsLegendPanel(MainFrame paramMainFrame) {
	super(paramMainFrame);
	initAll();
    }

    @Override
    public void initUi() {
	setOpaque(false);
	Model model = this.mainFrame.getBookModel();
	Session session = model.beginTransaction();
	StrandDAO dao = new StrandDAO(session);
	add(new JLabel(I18N.getMsg("report.caption.strands")), "gapright 5");
	for (Strand strand : dao.findAllOrderBySort()) {
	    JSLabel label = new JSLabel(strand.getName(), 0);
	    label.setPreferredSize(new Dimension(100, 20));
	    label.setBackground(ColorUtil.darker(strand.getJColor(), 0.05D));
	    add(label, "sg");
	}
	model.commit();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
	// empty
    }

}
