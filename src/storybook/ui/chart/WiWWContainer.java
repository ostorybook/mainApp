/*
 * SbApp: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package storybook.ui.chart;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import storybook.model.Model;
import storybook.model.hbn.dao.LocationDAO;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Person;
import storybook.tools.DateUtil;
import storybook.ui.MainFrame;

public class WiWWContainer {

    private MainFrame mainFrame;
    private Location location;
    private List<Person> inPersonList;
    private List<Person> outPersonList;
    private Date date;
    private boolean found;

    public WiWWContainer(MainFrame mainFrame, Date date, Location location, List<Person> persons) {
	this.mainFrame = mainFrame;
	this.location = location;
	this.inPersonList = persons;
	this.date = DateUtil.getZeroTimeDate(date);
	this.outPersonList = new ArrayList<>();
	init();
    }

    private void init() {
	for (Person person : this.inPersonList) {
	    Model model = this.mainFrame.getBookModel();
	    Session session = model.beginTransaction();
	    LocationDAO dao = new LocationDAO(session);
	    long l = dao.countByPersonLocationDate(person, this.location, this.date);
	    model.commit();
	    if (l != 0L) {
		this.outPersonList.add(person);
	    }
	}
	found = !outPersonList.isEmpty();
    }

    public List<Person> getCharacterList() {
	return this.outPersonList;
    }

    public boolean isFound() {
	return this.found;
    }
}
