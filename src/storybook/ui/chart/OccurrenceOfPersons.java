/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package storybook.ui.chart;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import org.hibernate.Session;
import storybook.model.Model;
import storybook.model.hbn.dao.PersonDAO;
import storybook.model.hbn.dao.SceneDAO;
import storybook.model.hbn.entity.Category;
import storybook.model.hbn.entity.Person;
import storybook.ui.MIG;
import storybook.ui.MainFrame;
import storybook.ui.chart.occurences.Dataset;
import storybook.ui.chart.occurences.DatasetItem;
import storybook.ui.chart.occurences.OccurencesPanel;

/**
 *
 * @author favdb
 */
public class OccurrenceOfPersons extends AbstractPersonsChart {

	private OccurencesPanel occurencesPanel;
	private Dataset dataset;

	public OccurrenceOfPersons(MainFrame mainFrame) {
		super(mainFrame, "report.person.occurrence.title");
	}

	@Override
	protected void initChartUi() {
		createOccurenceOfPersons();
		occurencesPanel = new OccurencesPanel(chartTitle, "value", " ", dataset);
		this.panel.add(this.occurencesPanel, MIG.GROW);
	}

	public List<Category> getSelectedCategories() {
		return (selectedCategories);
	}

	private void createOccurenceOfPersons() {
		//LOG.trace("OccurenceOfPersons.createOccurenceOfPersons()");
		dataset = new Dataset(mainFrame);
		dataset.items = new ArrayList<>();
		Model model = this.mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PersonDAO dao = new PersonDAO(session);
		List<Person> categories = dao.findByCategories(this.selectedCategories);
		SceneDAO scenes = new SceneDAO(session);
		double d = 0.0D;
		dataset.maxValue = 0L;
		dataset.listId = new ArrayList<>();
		for (Person person : categories) {
			long l = scenes.countByPerson(person);
			dataset.items.add(new DatasetItem(person.getAbbr(), l, person.getJColor()));
			if (dataset.maxValue < l) {
				dataset.maxValue = l;
			}
			dataset.listId.add(person.getAbbr());
			d += l;
		}
		model.commit();
		dataset.average = (d / dataset.items.size());
	}

	@Override
	public JPanel getPanelToExport() {
		return occurencesPanel;
	}

}
