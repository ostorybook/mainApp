/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package storybook.ui.chart;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import api.mig.swing.MigLayout;
import i18n.I18N;
import storybook.model.EntityUtil;
import storybook.model.hbn.entity.Person;
import storybook.ui.MIG;
import storybook.ui.MainFrame;

public class GanttChart extends AbstractPersonsChart {

	private JCheckBox cbShowPeriodsOfLife;
	private List<JCheckBox> personCbList;
	private JPanel personsPanel;

	public GanttChart(MainFrame paramMainFrame) {
		super(paramMainFrame, "chart.gantt.characters.title");
	}

	@Override
	protected void initChart() {
		super.initChart();
		refreshPersonCbList();
		this.cbShowPeriodsOfLife = new JCheckBox(I18N.getMsg("chart.gantt.periods.life"));
		this.cbShowPeriodsOfLife.setOpaque(false);
		this.cbShowPeriodsOfLife.setSelected(true);
		this.cbShowPeriodsOfLife.addActionListener(this);
	}

	@Override
	protected void initChartUi() {
		// empty
	}

	@Override
	protected void initOptionsUi() {
		super.initOptionsUi();
		this.optionsPanel.add(this.cbShowPeriodsOfLife, MIG.get(MIG.RIGHT, "gap push"));
		this.personsPanel = new JPanel(new MigLayout(MIG.FLOWX));
		this.personsPanel.setOpaque(false);
		this.panel.setOpaque(false);
		refreshPersonsPanel();
		this.optionsPanel.add(this.personsPanel, MIG.get(MIG.NEWLINE, MIG.SPAN));
	}

	@Override
	public void actionPerformed(ActionEvent paramActionEvent) {
		if ((paramActionEvent.getSource() instanceof JCheckBox)) {
			JCheckBox localJCheckBox = (JCheckBox) paramActionEvent.getSource();
			Object localObject = localJCheckBox.getClientProperty("CbCategory");
			if (localObject != null) {
				refreshPersonsPanel();
				return;
			}
			super.actionPerformed(paramActionEvent);
		}
	}

	private void refreshPersonCbList() {
		this.personCbList = EntityUtil.personsCreateCbList(this.mainFrame, this.categoryCbList, this);
	}

	private void refreshPersonsPanel() {
		List localList = getSelectedPersons();
		refreshPersonCbList();
		this.personsPanel.removeAll();
		int i = 0;
		for (JCheckBox localJCheckBox : this.personCbList) {
			Person localPerson = (Person) localJCheckBox.getClientProperty("CbPerson");
			if (localList.contains(localPerson)) {
				localJCheckBox.setSelected(true);
			}
			String str = "";
			if (i % 5 == 0) {
				str = "newline";
			}
			this.personsPanel.add(localJCheckBox, str);
			i++;
		}
		this.personsPanel.revalidate();
		this.personsPanel.repaint();
	}

	@SuppressWarnings("unchecked")
	private List<Person> getSelectedPersons() {
		ArrayList localArrayList = new ArrayList();
		for (JCheckBox localJCheckBox : this.personCbList) {
			if (localJCheckBox.isSelected()) {
				Person localPerson = (Person) localJCheckBox.getClientProperty("CbPerson");
				localArrayList.add(localPerson);
			}
		}
		return localArrayList;
	}

	@Override
	public JPanel getPanelToExport() {
		return personsPanel;
	}

}
