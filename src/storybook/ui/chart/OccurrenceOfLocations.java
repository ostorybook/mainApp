/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package storybook.ui.chart;

import i18n.I18N;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.hibernate.Session;
import api.mig.swing.MigLayout;
import storybook.ctrl.Ctrl;
import storybook.exim.EXIM;
import storybook.model.EntityUtil;
import storybook.model.Model;
import storybook.model.hbn.dao.LocationDAO;
import storybook.model.hbn.dao.SceneDAO;
import storybook.model.hbn.entity.Location;
import storybook.tools.swing.ColorUtil;
import storybook.tools.swing.PrintUtil;
import storybook.ui.MIG;
import storybook.ui.MainFrame;
import storybook.ui.chart.occurences.Dataset;
import storybook.ui.chart.occurences.DatasetItem;
import storybook.ui.chart.occurences.OccurencesPanel;

/**
 *
 * @author favdb
 */
public class OccurrenceOfLocations extends AbstractChartPanel implements ActionListener {

	private OccurencesPanel timelinePanel;
	private List<JCheckBox> countryCbList;
	protected List<String> selectedCountries;
	private Dataset dataset;

	public OccurrenceOfLocations(MainFrame mainFrame) {
		super(mainFrame, "report.location.occurrence.title");
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void initChart() {
		this.countryCbList = EntityUtil.countryCreateCbList(this.mainFrame, this);
		this.selectedCountries = new ArrayList();
		updateSelectedCountries();
	}

	@Override
	protected void initChartUi() {
		dataset = new Dataset(mainFrame);
		createOccurenceOfLocations();
		timelinePanel = new OccurencesPanel(chartTitle, "value", " ", dataset);
		this.panel.add(this.timelinePanel, MIG.GROW);
	}

	@Override
	protected void initOptionsUi() {
		JPanel localJPanel = new JPanel(new MigLayout(MIG.FLOWX));
		localJPanel.setOpaque(false);
		JLabel label = new JLabel(I18N.getColonMsg("location.country"));
		localJPanel.add(label);
		for (JCheckBox localJCheckBox : this.countryCbList) {
			localJPanel.add(localJCheckBox);
		}
		this.optionsPanel.add(localJPanel);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		switch (Ctrl.getPROPS(evt.getActionCommand())) {
			case EXPORT:
				EXIM.exporter(mainFrame, timelinePanel);
				return;
			case REFRESH:
				refresh();
				return;
			case PRINT:
				PrintUtil.printComponent(this);
				return;
			default:
				break;
		}
		updateSelectedCountries();
		refreshChart();
	}

	private void updateSelectedCountries() {
		this.selectedCountries.clear();
		for (JCheckBox localJCheckBox : this.countryCbList) {
			if (localJCheckBox.isSelected()) {
				this.selectedCountries.add(localJCheckBox.getText());
			}
		}
	}

	public List<String> getSelectedCountries() {
		return (selectedCountries);
	}

	private void createOccurenceOfLocations() {
		dataset = new Dataset(mainFrame);
		dataset.items = new ArrayList<>();
		Model localDocumentModel = this.mainFrame.getBookModel();
		Session localSession = localDocumentModel.beginTransaction();
		LocationDAO localLocationDAOImpl = new LocationDAO(localSession);
		SceneDAO localSceneDAOImpl = new SceneDAO(localSession);
		List<Location> localList = localLocationDAOImpl.findByCountries(selectedCountries);
		double d = 0.0D;
		Color[] color = ColorUtil.getNiceColors();
		dataset.maxValue = 0L;
		dataset.listId = new ArrayList<>();
		int ncolor = 0;
		for (Location location : localList) {
			long l = localSceneDAOImpl.countByLocation(location);
			dataset.items.add(new DatasetItem(location.getAbbr(), l, color[ncolor]));
			//LOG.trace("--> id=" + location.getAbbr() + ", value=" + l + ", color=" + color[ncolor].getRGB());
			if (dataset.maxValue < l) {
				dataset.maxValue = l;
			}
			dataset.listId.add(location.getAbbr());
			ncolor++;
			if (ncolor >= color.length) {
				ncolor = 0;
			}
			d += l;
		}
		localDocumentModel.commit();
		dataset.average = (d / localList.size());
	}

	@Override
	public JPanel getPanelToExport() {
		return timelinePanel;
	}

}
