/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package storybook.ui.chart;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.hibernate.Session;
import api.mig.swing.MigLayout;
import i18n.I18N;
import storybook.model.EntityUtil;
import storybook.model.Model;
import storybook.model.hbn.dao.ItemDAO;
import storybook.model.hbn.dao.SceneDAO;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.hbn.entity.Item;
import storybook.tools.swing.ColorUtil;
import storybook.ui.MIG;
import storybook.ui.MainFrame;
import storybook.ui.chart.occurences.Dataset;
import storybook.ui.chart.occurences.DatasetItem;
import storybook.ui.chart.occurences.OccurencesPanel;

/**
 *
 * @author favdb
 */
public class OccurrenceOfItems extends AbstractChartPanel {

    private OccurencesPanel timelinePanel;
    private List<JCheckBox> categoryCbList;
    protected List<String> selectedCategories;
    private Dataset dataset;

    public OccurrenceOfItems(MainFrame mainFrame) {
	super(mainFrame, "report.item.occurrence.title");
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void initChart() {
	this.categoryCbList = EntityUtil.itemCategoryCreateCbList(this.mainFrame, this);
	this.selectedCategories = new ArrayList();
	updateSelectedCategories();
    }

    @Override
    protected void initChartUi() {
	dataset = new Dataset(mainFrame);
	createOccurenceOfCategories();
	timelinePanel = new OccurencesPanel(chartTitle, "value", " ", dataset);
	this.panel.add(this.timelinePanel, MIG.GROW);
    }

    @Override
    protected void initOptionsUi() {
	JPanel localJPanel = new JPanel(new MigLayout(MIG.FLOWX));
	localJPanel.setOpaque(false);
	JLabel label = new JLabel(I18N.getColonMsg("category"));
	localJPanel.add(label);
	for (JCheckBox localJCheckBox : this.categoryCbList) {
	    localJPanel.add(localJCheckBox);
	}
	this.optionsPanel.add(localJPanel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
	updateSelectedCategories();
	refreshChart();
    }

    private void updateSelectedCategories() {
	this.selectedCategories.clear();
	for (JCheckBox localJCheckBox : this.categoryCbList) {
	    if (localJCheckBox.isSelected()) {
		this.selectedCategories.add(localJCheckBox.getText());
	    }
	}
    }

    public List<String> getSelectedCategories() {
	return (selectedCategories);
    }

    private void createOccurenceOfCategories() {
	dataset = new Dataset(mainFrame);
	dataset.items = new ArrayList<>();
	Model model = this.mainFrame.getBookModel();
	Session session = model.beginTransaction();
	ItemDAO dao = new ItemDAO(session);
	SceneDAO sceneDAO = new SceneDAO(session);
	List<AbstractEntity> entities = dao.findAllByCategory();
	double d = 0.0D;
	Color[] color = ColorUtil.getNiceColors();
	dataset.maxValue = 0L;
	dataset.listId = new ArrayList<>();
	int ncolor = 0;
	for (AbstractEntity entity : entities) {
	    if (selectedCategories.contains(((Item) entity).getCategory())) {
		long l = sceneDAO.countByItem((Item) entity);
		dataset.items.add(new DatasetItem(entity.getAbbr(), l, color[ncolor]));
		if (dataset.maxValue < l) {
		    dataset.maxValue = l;
		}
		dataset.listId.add(entity.getAbbr());
		ncolor++;
		if (ncolor >= color.length) {
		    ncolor = 0;
		}
		d += l;
	    }
	}
	model.commit();
	if (entities.size() > 0) {
	    dataset.average = (d / entities.size());
	}
    }

    @Override
    public JPanel getPanelToExport() {
	return (timelinePanel);
    }

}
