/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.table;

import i18n.I18N;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.JToolBar;
import org.hibernate.Session;
import storybook.ctrl.ActKey;
import storybook.model.EntityUtil;
import storybook.model.Model;
import storybook.model.book.Book;
import storybook.model.hbn.dao.EventDAO;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.hbn.entity.Categories;
import storybook.model.DB;
import storybook.model.hbn.entity.Event;
import storybook.ui.MainFrame;
import static storybook.ui.panel.AbstractPanel.ALL;
import static storybook.ui.panel.AbstractPanel.EMPTY;
import static storybook.ui.table.AbsColumn.*;

/**
 * @author favdb
 *
 */
@SuppressWarnings("serial")
public class EventTable extends AbstractTable {

	private static final String TT = "EventTable";

	public EventTable(MainFrame mainFrame) {
		super(mainFrame, Book.TYPE.EVENT);
	}

	@Override
	public void init() {
		this.withPart = false;
	}

	@Override
	public JToolBar initToolbar() {
		super.initToolbar();
		this.addCbCategories(Categories.find(mainFrame, getType()), null, !EMPTY, ALL);
		return toolbar;
	}

	@SuppressWarnings({"unchecked"})
	@Override
	public List<AbstractEntity> getAllEntities() {
		//LOG.trace(TT+".getAllEntities()");
		List list = EntityUtil.findEntities(mainFrame, getType());
		if (cbCategories != null && cbCategories.getSelectedIndex() > 0) {
			Model model = mainFrame.getBookModel();
			Session session = model.beginTransaction();
			EventDAO dao = new EventDAO(session);
			list = dao.findByCategory((String) cbCategories.getSelectedItem());
			model.commit();
		}
		return (list);
	}

	@Override
	protected void initTableModel(PropertyChangeEvent evt) {
		//LOG.trace(TT+".initTableModel(evt)");
		table.putClientProperty("MainFrame", mainFrame);
		for (int i = tableModel.getRowCount() - 1; i >= 0; i--) {
			tableModel.removeRow(i);
		}
		try {
			List<AbstractEntity> entities = getAllEntities();

			for (AbstractEntity entity : entities) {
				List<Object> cols = getRow(entity);
				tableModel.addRow(cols.toArray());
			}
		} catch (ClassCastException e) {
		}
		table.packAll();
	}

	@Override
	protected void modelPropertyChangeLocal(PropertyChangeEvent evt) {
		//LOG.trace(TT + ".modelPropertyChangeLocal(evt=" + evt + ")");
		ActKey act = new ActKey(evt);
		if (isInit(act)) {
			return;
		}
		if (Book.getTYPE(act.type) == Book.TYPE.EVENT && (act.isNew() || act.isUpdate())) {
			fillTable();
			reloadCategories();
		}
	}

	@Override
	protected AbstractEntity getEntity(Long id) {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		EventDAO dao = new EventDAO(session);
		Event scene = dao.find(id);
		model.commit();
		return scene;
	}

	@Override
	public List<AbsColumn> getColumns(AbstractEntity entity) {
		List<AbsColumn> cols = super.getColumns(entity);

		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.DATE, TCR_DATE, AL_CENTER));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.DURATION, AL_CENTER));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.EVENT_TIMESTEP, NUMERIC, TCR_HIDE, AL_CENTER));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.CATEGORY));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.COLOR, TCR_COLOR, AL_CENTER));

		this.getColumnsEnd(cols, entity);

		return (cols);
	}

	@Override
	public List<Object> getRow(AbstractEntity entity) {
		List<Object> cols = super.getRow(entity);
		Event e = (Event) entity;
		cols.add(e.getEventTime());
		cols.add(e.getDurationToText());
		if (e.getTimeStep() > 0) {
			cols.add(e.getTimestepToText());
		} else {
			cols.add("");
		}
		cols.add(e.getCategory());
		cols.add(e.getJColor());
		getRowEnd(cols, entity);
		return (cols);
	}

	@Override
	public void updateRow(AbstractEntity entity) {
		// nothing
	}

	@SuppressWarnings("unchecked")
	private void reloadCategories() {
		//LOG.trace(TT + ".reloadCategories()");
		int idx = cbCategories.getSelectedIndex();
		String n = (String) cbCategories.getSelectedItem();
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		EventDAO dao = new EventDAO(session);
		List<String> cats = dao.findCategories();
		model.commit();
		cbCategories.removeAllItems();
		cbCategories.addItem(I18N.getMsg("all"));
		String nx = "";
		for (String c : cats) {
			cbCategories.addItem(c);
			if (c.equals(n)) {
				nx = c;
			}
		}
		cbCategories.setSelectedIndex(0);
		if (idx > 0 && !nx.isEmpty()) {
			cbCategories.setSelectedItem(n);
		}
	}

	@Override
	protected void deleteEntity(AbstractEntity entity) {
		//LOG.trace(TT + ".deleteEntity(entity=" + AbstractEntity.trace(entity) + ")");
		reloadCategories();
		super.deleteEntity(entity);
	}

	@Override
	protected void newEntity(AbstractEntity entity) {
		//LOG.trace(TT + ".newEntity(entity=" + AbstractEntity.trace(entity) + ")");
		reloadCategories();
		super.newEntity(entity);
	}

	@Override
	protected void updateEntity(AbstractEntity entity) {
		//LOG.trace(TT + ".updateEntity(entity=" + AbstractEntity.trace(entity) + ")");
		reloadCategories();
		super.updateEntity(entity);
	}

}
