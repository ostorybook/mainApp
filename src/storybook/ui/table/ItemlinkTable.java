/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.table;

import java.beans.PropertyChangeEvent;
import java.util.List;
import org.hibernate.Session;
import storybook.ctrl.ActKey;
import storybook.model.Model;
import storybook.model.book.Book;
import storybook.model.hbn.dao.ItemlinkDAO;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.DB;
import storybook.model.hbn.entity.Itemlink;
import storybook.ui.MainFrame;
import static storybook.ui.table.AbsColumn.*;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class ItemlinkTable extends AbstractTable {

	public ItemlinkTable(MainFrame mainFrame) {
		super(mainFrame, Book.TYPE.ITEMLINK);
	}

	@Override
	public void init() {
		this.withPart = false;
	}

	@Override
	public void initUi() {
		super.initUi();
	}

	@Override
	protected void modelPropertyChangeLocal(PropertyChangeEvent evt) {
		ActKey act = new ActKey(evt);
		if (act.isUpdate()
				&& (act.getType() == Book.TYPE.ITEM
				|| act.getType() == Book.TYPE.ITEMLINK)) {
			fillTable();
		}
	}

	@Override
	protected AbstractEntity getEntity(Long id) {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		ItemlinkDAO dao = new ItemlinkDAO(session);
		Itemlink r = dao.find(id);
		model.commit();
		return r;
	}

	@Override
	public List<AbsColumn> getColumns(AbstractEntity entity) {
		List<AbsColumn> cols = super.getColumns(entity);

		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.ITEM, TCR_ENTITY));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.SCENE_START, TCR_ENTITY));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.SCENE_END, TCR_ENTITY));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.PERSON, TCR_ENTITY));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.LOCATION, TCR_ENTITY));

		this.getColumnsEnd(cols, entity);

		return (cols);
	}

	@Override
	public List<Object> getRow(AbstractEntity entity) {
		List<Object> cols = super.getRow(entity);
		Itemlink e = (Itemlink) entity;
		cols.add(e.getItem());
		cols.add(e.getStartScene());
		cols.add(e.getEndScene());
		cols.add(e.getPerson());
		cols.add(e.getLocation());
		getRowEnd(cols, entity);
		return cols;
	}

	@Override
	public void updateRow(AbstractEntity entity) {
		// refresh for scene, person, item, tag, location
		switch (Book.getTYPE(entity)) {
			case ITEM:
			case LOCATION:
			case PERSON:
			case SCENE:
			case TAG:
				fillTable();
				break;
			default:
				break;
		}
	}

}
