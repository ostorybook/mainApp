/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.table;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import storybook.ctrl.ActKey;
import storybook.model.Model;
import storybook.model.book.Book;
import storybook.model.hbn.dao.TaglinkDAO;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.DB;
import storybook.model.hbn.entity.Taglink;
import storybook.ui.MainFrame;
import static storybook.ui.table.AbsColumn.TCR_ENTITY;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class TaglinkTable extends AbstractTable {

	public TaglinkTable(MainFrame mainFrame) {
		super(mainFrame, Book.TYPE.TAGLINK);
	}

	@Override
	public void init() {
		this.withPart = false;
	}

	@Override
	public void initUi() {
		super.initUi();
	}

	@Override
	protected void modelPropertyChangeLocal(PropertyChangeEvent evt) {
		ActKey act = new ActKey(evt);
		if (act.isUpdate()
				&& (act.getType() == Book.TYPE.TAG
				|| act.getType() == Book.TYPE.TAGLINK)) {
			fillTable();
		}
	}

	@Override
	protected void sendSetEntityToEdit(int row) {
		if (row == -1) {
			return;
		}
		Taglink r = (Taglink) getEntityFromRow(row);
		if (r != null) {
			mainFrame.showEditorAsDialog(r);
		}
	}

	@Override
	protected void sendSetNewEntityToEdit(AbstractEntity entity) {
		mainFrame.showEditorAsDialog(entity);
	}

	@Override
	protected synchronized void sendDeleteEntity(int row) {
		Taglink r = (Taglink) getEntityFromRow(row);
		if (r != null) {
			ctrl.deleteEntity(r);
		}
	}

	@Override
	protected synchronized void sendDeleteEntities(List<AbstractEntity> entities) {
		ArrayList<Long> ids = new ArrayList<>();
		for (AbstractEntity entity : entities) {
			Taglink r = (Taglink) entity;
			ids.add(r.getId());
		}
		ctrl.deletemultiEntity(Book.TYPE.TAGLINK, ids);
	}

	@Override
	protected AbstractEntity getEntity(Long id) {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		TaglinkDAO dao = new TaglinkDAO(session);
		Taglink r = dao.find(id);
		model.commit();
		return r;
	}

	@Override
	public void updateRow(AbstractEntity entity) {
		// refresh for scene, person, item, tag, location
		switch (Book.getTYPE(entity)) {
			case ITEM:
			case LOCATION:
			case PERSON:
			case SCENE:
			case TAG:
				fillTable();
				break;
			default:
				break;
		}
	}

	@Override
	public List<AbsColumn> getColumns(AbstractEntity entity) {
		List<AbsColumn> cols = super.getColumns(entity);

		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.TAG, TCR_ENTITY));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.SCENE_START, TCR_ENTITY));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.SCENE_END, TCR_ENTITY));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.PERSON, TCR_ENTITY));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.LOCATION, TCR_ENTITY));

		this.getColumnsEnd(cols, entity);

		return (cols);
	}

	@Override
	public List<Object> getRow(AbstractEntity entity) {
		List<Object> cols = super.getRow(entity);
		Taglink e = (Taglink) entity;
		cols.add(e.getTag());
		cols.add(e.getStartScene());
		cols.add(e.getEndScene());
		cols.add(e.getPerson());
		cols.add(e.getLocation());
		getRowEnd(cols, entity);
		return (cols);
	}

}
