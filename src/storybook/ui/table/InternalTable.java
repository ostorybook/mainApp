/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.table;

import i18n.I18N;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JToolBar;
import org.hibernate.Session;
import storybook.model.Model;
import storybook.model.book.Book;
import storybook.model.hbn.dao.InternalDAO;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.DB;
import storybook.model.hbn.entity.Internal;
import storybook.tools.TextUtil;
import storybook.tools.swing.FontUtil;
import storybook.ui.MainFrame;
import static storybook.ui.table.AbsColumn.*;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class InternalTable extends AbstractTable {

	public InternalTable(MainFrame mainFrame) {
		super(mainFrame, Book.TYPE.INTERNAL);
	}

	@Override
	public void init() {
		this.withPart = false;
	}

	@Override
	public void initUi() {
		super.initUi();
	}

	@Override
	public JToolBar initToolbar() {
		super.initToolbar();
		toolbar.setBackground(Color.RED);
		JLabel lb = new JLabel(I18N.getMsg("internal.table_msg"));
		lb.setFont(FontUtil.getBold(lb.getFont()));
		lb.setForeground(Color.WHITE);
		toolbar.add(lb);
		return toolbar;
	}

	@Override
	protected void modelPropertyChangeLocal(PropertyChangeEvent evt) {
		//no specific change
	}

	@Override
	protected AbstractEntity getEntity(Long id) {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		InternalDAO dao = new InternalDAO(session);
		Internal internal = dao.find(id);
		model.commit();
		return internal;
	}

	@Override
	public void updateRow(AbstractEntity entity) {
		// nothing
	}

	@Override
	public List<AbsColumn> getColumns(AbstractEntity entity) {
		List<AbsColumn> cols = new ArrayList<>();

		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.ID, NUM_LONG, TCR_HIDE, AL_CENTER));
		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.INTERNAL_KEY));
		cols.add(new AbsColumn(mainFrame, cols, "attribute.value"));

		getColumnsEnd(cols, entity);

		return (cols);
	}

	@Override
	public List<Object> getRow(AbstractEntity entity) {
		List<Object> row = new ArrayList<>();
		Internal e = (Internal) entity;
		row.add(e.getId());
		row.add(e.getKey());
		if (e.getStringValue() != null && !e.getStringValue().isEmpty()) {
			row.add(TextUtil.ellipsize(e.getStringValue(), 128));
		} else if (e.getIntegerValue() != null && e.getIntegerValue() > 1) {
			row.add(e.getIntegerValue());
		} else if (e.getBooleanValue() != null && e.getBooleanValue() == true) {
			row.add("true");
		} else if (e.getBinValue() != null && e.getBinValue().length > 0) {
			row.add("[" + (e.getBinValue().length) + "]");
		} else {
			row.add("");
		}
		return (row);
	}

}
