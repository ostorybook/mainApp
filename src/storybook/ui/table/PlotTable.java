/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.table;

import i18n.I18N;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.JToolBar;
import org.hibernate.Session;
import storybook.ctrl.ActKey;
import storybook.model.EntityUtil;
import storybook.model.Model;
import storybook.model.book.Book;
import storybook.model.hbn.dao.PlotDAO;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.hbn.entity.Categories;
import storybook.model.DB;
import storybook.model.hbn.entity.Plot;
import storybook.ui.MainFrame;
import static storybook.ui.panel.AbstractPanel.ALL;
import static storybook.ui.panel.AbstractPanel.EMPTY;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class PlotTable extends AbstractTable {

	private static final String TT = "PlotTable";

	public PlotTable(MainFrame mainFrame) {
		super(mainFrame, Book.TYPE.PLOT);
	}

	@Override
	public void init() {
		this.withPart = false;
		allowMultiDelete = false;
	}

	@Override
	public JToolBar initToolbar() {
		super.initToolbar();
		addCbCategories(Categories.find(mainFrame, getType()), null, !EMPTY, ALL);
		return toolbar;
	}

	@SuppressWarnings({"unchecked"})
	@Override
	public List<AbstractEntity> getAllEntities() {
		//LOG.trace(TT+".getAllEntities()");
		if (cbCategories != null && cbCategories.getSelectedIndex() > 0) {
			Model model = mainFrame.getBookModel();
			Session session = model.beginTransaction();
			PlotDAO dao = new PlotDAO(session);
			List list = dao.findByCategory((String) cbCategories.getSelectedItem());
			model.commit();
			return (list);
		} else {
			return (EntityUtil.findEntities(mainFrame, getType()));
		}
	}

	@Override
	protected void modelPropertyChangeLocal(PropertyChangeEvent evt) {
		ActKey act = new ActKey(evt);
		if (act.isUpdate()
			&& (act.getType() == Book.TYPE.PLOT)) {
			fillTable();
		}
	}

	@Override
	protected AbstractEntity getEntity(Long id) {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PlotDAO dao = new PlotDAO(session);
		Plot plot = dao.find(id);
		model.commit();
		return plot;
	}

	@Override
	public void updateRow(AbstractEntity entity) {
		// nothing
	}

	@Override
	public List<AbsColumn> getColumns(AbstractEntity entity) {
		List<AbsColumn> cols = super.getColumns(entity);

		cols.add(new AbsColumn(mainFrame, cols, DB.DATA.CATEGORY));

		this.getColumnsEnd(cols, entity);

		return (cols);
	}

	@Override
	public List<Object> getRow(AbstractEntity entity) {
		List<Object> cols = super.getRow(entity);
		Plot e = (Plot) entity;
		cols.add(e.getCategory());
		getRowEnd(cols, entity);
		return (cols);
	}

	@SuppressWarnings("unchecked")
	private void reloadCategories() {
		//LOG.trace(TT + ".reloadCategories()");
		int idx = cbCategories.getSelectedIndex();
		String n = (String) cbCategories.getSelectedItem();
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PlotDAO dao = new PlotDAO(session);
		List<String> cats = dao.findCategories();
		model.commit();
		cbCategories.removeAllItems();
		cbCategories.addItem(I18N.getMsg("all"));
		String nx = "";
		for (String c : cats) {
			cbCategories.addItem(c);
			if (c.equals(n)) {
				nx = c;
			}
		}
		cbCategories.setSelectedIndex(0);
		if (idx > 0 && !nx.isEmpty()) {
			cbCategories.setSelectedItem(n);
		}
	}

	@Override
	protected void deleteEntity(AbstractEntity entity) {
		//LOG.trace(TT + ".deleteEntity(entity=" + AbstractEntity.trace(entity) + ")");
		reloadCategories();
		super.deleteEntity(entity);
	}

	@Override
	protected void newEntity(AbstractEntity entity) {
		//LOG.trace(TT + ".newEntity(entity=" + AbstractEntity.trace(entity) + ")");
		reloadCategories();
		super.newEntity(entity);
	}

	@Override
	protected void updateEntity(AbstractEntity entity) {
		//LOG.trace(TT + ".updateEntity(entity=" + AbstractEntity.trace(entity) + ")");
		reloadCategories();
		super.updateEntity(entity);
	}

}
