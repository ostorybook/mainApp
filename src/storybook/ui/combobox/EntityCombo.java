/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.combobox;

import java.awt.Dimension;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import org.hibernate.Session;
import i18n.I18N;
import storybook.model.Model;
import storybook.model.hbn.dao.*;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.book.Book;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class EntityCombo extends JComboBox {

	private final MainFrame mainFrame;
	private final Long toSel;
	private final Long toHide;
	private final Book.TYPE type;
	private final String options;

	/**
	 * Create a JComboBox for entities
	 *
	 * @param mainFrame
	 * @param type: type of entities
	 * @param toSel: Id to select
	 * @param toHide: entity to hide
	 * @param options : parameters, first for adding empty item, second for adding all item
	 */
	public EntityCombo(MainFrame mainFrame, Book.TYPE type, Long toSel,
			AbstractEntity toHide, String options) {
		super();
		setMinimumSize(new Dimension(150, 10));
		this.setMaximumRowCount(10);
		this.mainFrame = mainFrame;
		setName("cbScenes");
		this.type = type;
		this.toSel = toSel;
		if (toHide == null) {
			this.toHide = -1L;
		} else {
			this.toHide = toHide.getId();
		}
		String px = options;
		while (px.length() < 3) {
			px += "0";
		}
		this.options = options;
		reload();
	}

	@SuppressWarnings("unchecked")
	public void reload() {
		int curSel = this.getSelectedIndex();
		DefaultComboBoxModel cbModel = (DefaultComboBoxModel) this.getModel();
		cbModel.removeAllElements();
		if (isAddEmpty(options)) {
			cbModel.addElement((String) "");
		}
		EntityComboEntity memo = null;
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		List entities;
		switch (type) {
			case ATTRIBUTE:
				entities = new AttributeDAO(session).findAll();
				break;
			case CATEGORY:
				entities = new CategoryDAO(session).findAll();
				break;
			case CHAPTER:
				entities = new ChapterDAO(session).findAll();
				break;
			case ENDNOTES:
				entities = new EndnoteDAO(session).findAll();
				break;
			case EVENT:
				entities = new EventDAO(session).findAll();
				break;
			case GENDER:
				entities = new GenderDAO(session).findAll();
				break;
			case IDEA:
				entities = new IdeaDAO(session).findAll();
				break;
			case ITEM:
				entities = new ItemDAO(session).findAll();
				break;
			case ITEMLINK:
				entities = new ItemlinkDAO(session).findAll();
				break;
			case LOCATION:
				entities = new LocationDAO(session).findAll();
				break;
			case MEMO:
				entities = new MemoDAO(session).findAll();
				break;
			case PART:
				entities = new PartDAO(session).findAll();
				break;
			case PERSON:
				entities = new PersonDAO(session).findAll();
				break;
			case PLOT:
				entities = new PlotDAO(session).findAll();
				break;
			case RELATION:
				entities = new RelationDAO(session).findAll();
				break;
			case SCENE:
				entities = new SceneDAO(session).findAll();
				break;
			case STRAND:
				entities = new StrandDAO(session).findAll();
				break;
			case TAG:
				entities = new TagDAO(session).findAll();
				break;
			case TAGLINK:
				entities = new TaglinkDAO(session).findAll();
				break;
			default:
				return;
		}
		for (Object obj : entities) {
			AbstractEntity entity = (AbstractEntity) obj;
			if (!entity.getId().equals(toHide)) {
				EntityComboEntity cbEntity = new EntityComboEntity(entity);
				cbModel.addElement(cbEntity);
				if (toSel != -1L && toSel.equals(entity.getId())) {
					memo = cbEntity;
				}
			}
		}
		if (isAddAll(options)) {
			this.addItem(I18N.getMsg("all"));
		}
		if (toSel != -1L && memo != null) {
			this.setSelectedItem(memo);
		} else if (curSel != -1) {
			this.setSelectedIndex(curSel);
		}
		this.setModel(cbModel);
		model.commit();
	}

	private static boolean isAddEmpty(String x) {
		if (x.length()<1) return false;
		return x.charAt(0) == '1';
	}

	private static boolean isAddAll(String x) {
		if (x.length()<2) return false;
		return x.charAt(1) == '1';
	}

}
