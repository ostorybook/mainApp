/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.options;

import api.mig.swing.MigLayout;
import i18n.I18N;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import resources.icons.IconUtil;
import storybook.ui.MIG;
import storybook.ui.MainFrame;
import storybook.ui.SbView;
import storybook.ui.dialog.AbstractDialog;

/**
 *
 * @author favdb
 */
public class OptionsDlg extends AbstractDialog {

	private final String sbView;
	private OptBook bookOpt;
	private OptChrono chronoOpt;
	private OptManage manageOpt;
	private OptTimeline timelineOpt;

	public OptionsDlg(MainFrame m) {
		super(m);
		sbView = null;
		initAll();
	}

	public OptionsDlg(MainFrame m, String v) {
		super(m);
		sbView = v;
		initAll();
	}

	public static void show(MainFrame m, String v) {
		OptionsDlg dlg = new OptionsDlg(m, v);
		dlg.setVisible(true);
	}

	@Override
	public void init() {
		// empty
	}

	@Override
	public void initUi() {
		bookOpt = new OptBook(mainFrame);
		chronoOpt = new OptChrono(mainFrame);
		manageOpt = new OptManage(mainFrame);
		timelineOpt = new OptTimeline(mainFrame);
		//layout
		setLayout(new MigLayout(MIG.get(MIG.FILL, MIG.WRAP1)));
		setTitle(I18N.getMsg("options"));
		setIconImage(IconUtil.getIconImage("icon"));
		if (sbView == null) {
			JTabbedPane tabbed = new JTabbedPane();
			tabbed.add(I18N.getMsg("view.book"), bookOpt);
			tabbed.add(I18N.getMsg("view.chrono"), chronoOpt);
			tabbed.add(I18N.getMsg("view.manage"), manageOpt);
			add(tabbed);
		} else {
			switch (SbView.getVIEW(sbView)) {
				case BOOK:
					setTitle(I18N.getMsg("view.book"));
					add(bookOpt);
					break;
				case CHRONO:
					setTitle(I18N.getMsg("view.chrono"));
					add(chronoOpt);
					break;
				case MANAGE:
					setTitle(I18N.getMsg("view.manage"));
					add(manageOpt);
					break;
				case TIMELINE:
					setTitle(I18N.getMsg("view.timeline"));
					add(timelineOpt);
					break;
			}
		}
		add(getCloseButton(), MIG.get(MIG.SPAN, MIG.SG, MIG.RIGHT));
		pack();
		setLocationRelativeTo(mainFrame);
		setModal(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// empty
	}

}
