/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.panel.typist;

import i18n.I18N;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import api.mig.swing.MigLayout;
import resources.icons.ICONS;
import resources.icons.IconUtil;
import storybook.model.hbn.entity.Item;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Person;
import storybook.model.hbn.entity.Plot;
import storybook.model.hbn.entity.Scene;
import storybook.tools.html.Html;
import storybook.tools.swing.js.JSLabel;
import storybook.ui.MIG;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class TypistInfo extends JPanel {

	private static final String TT = "TypistInfo";

	private Scene scene;
	private boolean modified;
	private JPanel pPersons;
	private JPanel pLocations;
	private JPanel pItems;
	private JPanel pPlots;
	private JEditorPane edNotes, edDescription;
	private final MainFrame mainFrame;
	private final TypistPanel typist;
	private JScrollPane scrollerNotes;
	private JScrollPane scrollerDescription;

	public TypistInfo(TypistPanel m, Scene s) {
		typist = m;
		mainFrame = m.getMainFrame();
		scene = s;
		init();
		if (scene != null) {
			refreshInfo(false);
		}
		modified = false;
	}

	public void init() {
		String growxWrap = MIG.get(MIG.GROWX);
		setLayout(new MigLayout("ins 2, wrap", "[]"));

		add(new JLabel(" "));
		add(new JLabel(" "));
		add(new JLabel(" "));

		JButton btEdit = initButton("person");
		btEdit.setPreferredSize(new Dimension(Integer.MAX_VALUE, Integer.MIN_VALUE));
		add(btEdit, growxWrap);
		pPersons = new JPanel();
		pPersons.setLayout(new MigLayout(MIG.INS0));
		add(pPersons, growxWrap);

		btEdit = initButton("location");
		add(btEdit, growxWrap);
		pLocations = new JPanel();
		pLocations.setLayout(new MigLayout(MIG.get(MIG.INS0)));
		add(pLocations, growxWrap);

		btEdit = initButton("item");
		add(btEdit, growxWrap);
		pItems = new JPanel();
		pItems.setLayout(new MigLayout(MIG.get(MIG.INS0)));
		add(pItems, growxWrap);

		btEdit = initButton("plot");
		add(btEdit, growxWrap);
		pPlots = new JPanel();
		pPlots.setLayout(new MigLayout(MIG.get(MIG.INS0)));
		add(pPlots, growxWrap);

		btEdit = initButton("description");
		add(btEdit, growxWrap);
		edDescription = new JEditorPane(Html.TYPE, "");
		edDescription.setEditable(false);
		edDescription.setBackground(null);
		scrollerDescription = new JScrollPane(edDescription);
		scrollerDescription.setBorder(BorderFactory.createEmptyBorder());
		scrollerDescription.setMaximumSize(Toolkit.getDefaultToolkit().getScreenSize());;
		add(scrollerDescription, MIG.get(MIG.GROWY, growxWrap));

		btEdit = initButton("notes");
		add(btEdit, growxWrap);
		edNotes = new JEditorPane(Html.TYPE, "");
		edNotes.setEditable(false);
		edNotes.setBackground(null);
		scrollerNotes = new JScrollPane(edNotes);
		scrollerNotes.setBorder(BorderFactory.createEmptyBorder());
		scrollerNotes.setMaximumSize(Toolkit.getDefaultToolkit().getScreenSize());
		add(scrollerNotes, MIG.get(MIG.GROWY, growxWrap));
	}

	private JButton initButton(String entity) {
		JButton bt = new JButton(IconUtil.getIconSmall(ICONS.K.EDIT));
		bt.setText(I18N.getMsg(entity));
		bt.setToolTipText(I18N.getMsg(entity + ".edit"));
		bt.setMargin(new Insets(0, 0, 0, 0));
		bt.setHorizontalAlignment(SwingConstants.LEADING);
		bt.setHorizontalTextPosition(SwingConstants.LEADING);
		switch (entity) {
			case "person":
				bt.addActionListener((ActionEvent evt) -> {
					btPersonAction();
				});
				break;
			case "location":
				bt.addActionListener((ActionEvent evt) -> {
					btLocationAction();
				});
				break;
			case "item":
				bt.addActionListener((ActionEvent evt) -> {
					btItemAction();
				});
				break;
			case "plot":
				bt.addActionListener((ActionEvent evt) -> {
					btPlotAction();
				});
				break;
			case "description":
				bt.addActionListener((ActionEvent evt) -> {
					btDescriptionAction();
				});
				break;
			case "notes":
				bt.addActionListener((ActionEvent evt) -> {
					btNotesAction();
				});
				break;
			default:
				break;
		}
		return bt;
	}

	private void btPersonAction() {
		TypistEntitiesListDlg dlg = new TypistEntitiesListDlg(typist, scene, "person");
		dlg.setModal(true);
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.setPersons(dlg.getPersons());
			refreshInfo(true);
			modified = true;
		}
	}

	private void btLocationAction() {
		TypistEntitiesListDlg dlg = new TypistEntitiesListDlg(typist, scene, "location");
		dlg.setModal(true);
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.setLocations(dlg.getLocations());
			refreshInfo(true);
			modified = true;
		}
	}

	private void btItemAction() {
		TypistEntitiesListDlg dlg = new TypistEntitiesListDlg(typist, scene, "item");
		dlg.setModal(true);
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.setItems(dlg.getItems());
			refreshInfo(true);
			modified = true;
		}
	}

	private void btPlotAction() {
		TypistEntitiesListDlg dlg = new TypistEntitiesListDlg(typist, scene, "plot");
		dlg.setModal(true);
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.setPlots(dlg.getPlots());
			refreshInfo(true);
			modified = true;
		}
	}

	private void btDescriptionAction() {
		//LOG.trace(TT + ".btDescriptionAction()");
		TypistEditText dlg = new TypistEditText(typist, "description", scene.getDescription());
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.setDescription(dlg.getText());
			refreshInfo(true);
			modified = true;
		}
	}

	private void btNotesAction() {
		//LOG.trace(TT + ".btNotesAction()");
		TypistEditText dlg = new TypistEditText(typist, "notes", scene.getNotes());
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.setNotes(dlg.getText());
			refreshInfo(true);
			modified = true;
		}
	}

	public void refreshInfo(boolean toUpdate) {
		pPersons.removeAll();
		if (!scene.getPersons().isEmpty()) {
			pPersons.setBorder(javax.swing.BorderFactory.createEtchedBorder());
			for (Person p : scene.getPersons()) {
				pPersons.add(new JSLabel(p.getFullName()), MIG.WRAP);
			}
		} else {
			pPersons.setBorder(null);
		}

		pLocations.removeAll();
		if (!scene.getLocations().isEmpty()) {
			pLocations.setBorder(javax.swing.BorderFactory.createEtchedBorder());
			for (Location p : scene.getLocations()) {
				pLocations.add(new JSLabel(p.getName()), MIG.WRAP);
			}
		} else {
			pLocations.setBorder(null);
		}

		pItems.removeAll();
		if (!scene.getItems().isEmpty()) {
			pItems.setBorder(javax.swing.BorderFactory.createEtchedBorder());
			for (Item p : scene.getItems()) {
				pItems.add(new JSLabel(p.getName()), MIG.WRAP);
			}
		} else {
			pItems.setBorder(null);
		}

		pPlots.removeAll();
		if (!scene.getPlots().isEmpty()) {
			pPlots.setBorder(javax.swing.BorderFactory.createEtchedBorder());
			for (Plot p : scene.getPlots()) {
				pPlots.add(new JSLabel(p.getName()), MIG.WRAP);
			}
		} else {
			pPlots.setBorder(null);
		}

		String text = Html.htmlToText(scene.getNotes());
		edNotes.setText(scene.getNotes());
		if (!text.isEmpty()) {
			edNotes.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		} else {
			edNotes.setBorder(null);
		}
		edNotes.setCaretPosition(0);

		text = Html.htmlToText(scene.getDescription());
		edDescription.setText(scene.getDescription());
		if (!text.isEmpty()) {
			edDescription.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		} else {
			edDescription.setBorder(null);
		}
		edDescription.setCaretPosition(0);

		this.revalidate();
		if (toUpdate) {
			typist.updatedSet();
		}
	}

	void refreshInfo(Scene s) {
		scene = s;
		modified = false;
		refreshInfo(false);
	}

	public MainFrame getMainFrame() {
		return (((TypistPanel) getParent()).getMainFrame());
	}

}
