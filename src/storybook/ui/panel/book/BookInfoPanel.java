/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.panel.book;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.JLabel;
import api.mig.swing.MigLayout;
import storybook.ctrl.ActKey;
import storybook.ctrl.Ctrl.PROPS;
import storybook.model.EntityUtil;
import storybook.model.book.Book;
import storybook.model.hbn.entity.Scene;
import storybook.model.hbn.entity.Strand;
import storybook.tools.swing.SwingUtil;
import storybook.tools.swing.js.JSLabel;
import storybook.ui.MIG;
import storybook.ui.MainFrame;
import storybook.ui.panel.AbstractPanel;
import storybook.ui.panel.EntityLinksPanel;
import storybook.ui.panel.chrono.StrandDateLabel;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class BookInfoPanel extends AbstractPanel {

	private Scene scene;
	private JSLabel lbStrand;
	private EntityLinksPanel personLinksPanel, itemLinksPanel, locationLinksPanel, strandLinksPanel;
	private StrandDateLabel lbDate;

	public BookInfoPanel(MainFrame mainFrame, Scene scene) {
		super(mainFrame);
		this.scene = scene;
		init();
		initUi();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		Object newValue = evt.getNewValue();
		ActKey act = new ActKey(evt);
		switch (Book.getTYPE(act.type)) {
			case STRAND:
				if (PROPS.UPDATE.check(act.getCmd())) {
					Strand newStrand = (Strand) newValue;
					if (scene.getStrand() != null && newStrand.getId().equals(scene.getStrand().getId())) {
						lbStrand.setText(newStrand.toString());
						lbStrand.setBackground(newStrand.getJColor());
					}
					strandLinksPanel.refresh();
				}
				break;
			case SCENE:
				if (PROPS.UPDATE.check(act.getCmd())) {
					Scene newScene = (Scene) newValue;
					if (!newScene.getId().equals(scene.getId())) {
						return;
					}
					lbDate.setDate(newScene.getScenets());
					lbDate.refresh();
					if (newScene.getStrand() != null) {
						lbStrand.setText(newScene.getStrand().toString());
						lbStrand.setBackground(Strand.getJColor(newScene.getStrand()));
					} else {
						Strand strand = (Strand) EntityUtil.findEntities(mainFrame, Book.TYPE.STRAND).get(0);
						lbStrand.setText(strand.toString());
						lbStrand.setBackground(strand.getJColor());
					}
					personLinksPanel.refresh();
					itemLinksPanel.refresh();
					locationLinksPanel.refresh();
					strandLinksPanel.refresh();
				}
				break;
			default:
				break;
		}
	}

	@Override
	public void init() {
		// empty
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout(MIG.get(MIG.FILLX, MIG.WRAP, MIG.HIDEMODE3, MIG.INS1, MIG.GAP1)));
		setOpaque(false);
		setBorder(SwingUtil.getBorderDefault());
		// strand
		if (scene.getStrand() == null) {
			scene.setStrand((Strand) EntityUtil.findEntities(mainFrame, Book.TYPE.STRAND).get(0));
		}
		lbStrand = new JSLabel(scene.getStrand().toString(), JLabel.CENTER);
		lbStrand.setBackground(scene.getStrand().getJColor());
		add(lbStrand, MIG.GROWX);
		// date
		lbDate = new StrandDateLabel(scene.getStrand(), scene.getScenets(), false);
		lbDate.setOpaque(false);
		add(lbDate, MIG.CENTER);
		//lbDate.setVisible(scene.getScenets() != null);
		// person links
		String migdef = MIG.get("aligny top", MIG.SPLIT2);
		//add(new JLabel(IconUtil.getIconSmall(ICONS.K.ENT_PERSON)), migdef);
		personLinksPanel = new EntityLinksPanel(mainFrame, scene, Book.TYPE.PERSON, false);
		if (!scene.getPersons().isEmpty()) {
			add(personLinksPanel, MIG.GROWX);
		}
		// location links
		//add(new JLabel(IconUtil.getIconSmall(ICONS.K.ENT_LOCATION)), migdef);
		locationLinksPanel = new EntityLinksPanel(mainFrame, scene, Book.TYPE.LOCATION, false);
		if (!scene.getLocations().isEmpty()) {
			add(locationLinksPanel, MIG.GROWX);
		}
		// item links
		//add(new JLabel(IconUtil.getIconSmall(ICONS.K.ENT_ITEM)), migdef);
		itemLinksPanel = new EntityLinksPanel(mainFrame, scene, Book.TYPE.ITEM, false);
		if (!scene.getItems().isEmpty()) {
			add(itemLinksPanel, MIG.GROWX);
		}
		// strand links
		//add(new JLabel(I18N.getColonMsg("scene.strand.links")), MIG.SPLIT2);
		strandLinksPanel = new EntityLinksPanel(mainFrame, scene, Book.TYPE.STRAND, false);
		if (!scene.getStrands().isEmpty()) {
			add(strandLinksPanel, MIG.GROWX);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// empty
	}

}
