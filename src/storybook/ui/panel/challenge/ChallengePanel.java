/*
 * Copyright (C) 2023 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package storybook.ui.panel.challenge;

import api.mig.swing.MigLayout;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import storybook.tools.swing.js.JSTable;
import storybook.ui.MainFrame;
import storybook.ui.panel.AbstractPanel;

/**
 * todo challenge NaNoWrimo sur une idée de Profsamedi sur JE
 * https://www.jeunesecrivains.com/t63749-outils-logiciels-tableau-de-bord-d-avancement-nanowrimo
 * (fr)
 *
 * @author favdb
 */
public class ChallengePanel extends AbstractPanel {

	private static final String TT = "ChallengePanel";
	private Date start = new Date();
	private int duration = 30;
	private int objective = 10000;
	private static List<ChallengeData> data = new ArrayList<>();

	public ChallengePanel(MainFrame mainFrame) {
		super(mainFrame);
	}

	@Override
	public void init() {
		loadData();
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("", "[30%][70%]"));
		initToolbar();
		JSTable table = new JSTable();
		initTable();
		JScrollPane scroll = new JScrollPane(table);
		add(scroll);
		add(initRight());
	}

	/**
	 * tool bar to create the challenge
	 *
	 * @return
	 */
	@Override
	public JToolBar initToolbar() {
		JToolBar toolb = super.initToolbar();
		// button to create a new challenge, replacing the existing one if exists
		// button to reset the current challenge, if exixts
		return toolb;
	}

	/**
	 * initalize the table
	 */
	private void initTable() {
		// day
		// date
		// nb words
		// total
	}

	/**
	 * initialize the right panel
	 *
	 * @return
	 */
	private JPanel initRight() {
		JPanel panel = new JPanel(new MigLayout());
		// start date
		// duration
		// objective
		// minimum writed words by day
		// maximum writed words by day
		// medium writed words
		// remainig days
		// % of words writed
		// final date
		// % remaining words to write
		// previsional days remaining
		// words to write by day
		// progress	message
		// graph representing writed words, words to write
		return panel;
	}

	public void setStart(Date date) {
		if (date == null) {
			this.start = new Date();
		} else {
			this.start = date;
		}
	}

	public Date getStart() {
		return this.start;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getDuration() {
		return this.duration;
	}

	public void setObjective(int objective) {
		this.objective = objective;
	}

	public int getObjective() {
		return this.objective;
	}

	/**
	 * refresh
	 */
	public void Refresh() {
		// refresh the table
		// refresh other data
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		// empty
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// empty
	}

	/**
	 * load the data challenge from a file
	 */
	private void loadData() {
		// todo
		// read the specific Entity (Tag with type ??)
		// split the notes by lines
		// first line contains the objective of the challenge
		// like: date to start, duration in nb of days, total nb of words
		// for each other lines : (split on semi-colon) date and number of words (jj/mm/aaaa;nb)
	}

	/**
	 * compute data when a new day start
	 *
	 * @param mainFrame
	 * @return
	 */
	public static int computeData(MainFrame mainFrame) {
		int total = 0;
		for (ChallengeData c : data) {
			total += c.getNb();
		}
		return total;
	}

}
