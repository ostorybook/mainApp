/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.renderer.tcr;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import org.hibernate.Session;
import storybook.model.Model;
import storybook.model.hbn.entity.Category;
import storybook.ui.MainFrame;

/**
 * Table cell renderer for a Category entity
 *
 * @author favdb
 */
@SuppressWarnings("serial")
public class TCRCategory extends DefaultTableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		MainFrame mainFrame = (MainFrame) table.getClientProperty("MainFrame");
		JLabel lbText = (JLabel) super.getTableCellRendererComponent(table,
				null, isSelected, hasFocus, row, column);
		if (value instanceof Category) {
			Model model = mainFrame.getBookModel();
			Session session = model.beginTransaction();
			Long id = ((Category) value).getId();
			Category category = (Category) session.get(Category.class, id);
			model.commit();
			lbText.setText(category.toString());
		} else {
			if (value instanceof String) {
				lbText.setText(value.toString());
			}
		}
		return lbText;

	}
}
