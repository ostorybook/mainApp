/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.renderer.tcr;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import org.hibernate.Session;
import resources.icons.ICONS;
import resources.icons.IconUtil;
import storybook.App;
import storybook.model.Model;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.hbn.entity.Attribute;
import storybook.model.hbn.entity.Strand;
import storybook.tools.ListUtil;
import storybook.tools.swing.ColorIcon;
import storybook.ui.MainFrame;

/**
 * table cell renderer for an entity or a list of entities
 *
 * @author favdb
 */
public class TCREntities extends DefaultTableCellRenderer {

	MainFrame mainFrame;

	public TCREntities(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
		boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel label = (JLabel) super.getTableCellRendererComponent(table,
			"", isSelected, hasFocus, row, column);
		label.setToolTipText((""));
		if (value == null) {
			label.setText("");
		} else {
			if (value instanceof Icon) {
				setIcon(label, value);
			} else if (value instanceof String) {
				setString(label, value);
			} else if (value instanceof AbstractEntity) {
				setEntity(label, (AbstractEntity) value);
			} else if (value instanceof List) {
				List list = (List) value;
				if (list.isEmpty()) {
					label.setIcon(IconUtil.getIconSmall(ICONS.K.EMPTY));
					label.setText("");
					label.setToolTipText("");
				} else {
					setList(label, list);
				}
			}
		}
		return label;
	}

	private void setStrand(JLabel label, Object value) {
		label.setHorizontalAlignment(CENTER);
		int sz = App.getInstance().fonts.defGet().getSize();
		label.setIcon(new ColorIcon(Strand.getJColor((Strand) value), sz, sz * 2));
		label.setText("");
		label.setToolTipText(((Strand) value).getName());
	}

	private void setIcon(JLabel label, Object value) {
		label.setText("");
		label.setIcon((Icon) value);
	}

	private void setString(JLabel label, Object value) {
		String str = (String) value;
		if (str.isEmpty() || str.trim().equals("[]")) {
			label.setText("");
		} else {
			label.setText(str);
		}
	}

	private void setEntity(JLabel label, AbstractEntity entity) {
		if (entity == null) {
			return;
		}
		if (entity instanceof Strand) {
			setStrand(label, entity);
		} else {
			try {
				if (!(entity instanceof Attribute)) {
					label.setIcon(entity.getIcon());
				}
				label.setText(entity.getName());
			} catch (Exception ex) {

			}
		}
	}

	private void setList(JLabel label, List value) {
		@SuppressWarnings("unchecked")
		List<AbstractEntity> list = (List<AbstractEntity>) value;
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		AbstractEntity ent = list.get(0);
		session.refresh(ent);
		if (list.size() < 2) {
			setEntity(label, list.get(0));
		} else {
			List<String> tips = new ArrayList<>();
			for (AbstractEntity entity : list) {
				tips.add(entity.getName());
			}
			label.setText(ListUtil.join(tips));
			label.setToolTipText(ListUtil.join(tips, ", "));
		}
		model.commit();
	}

}
