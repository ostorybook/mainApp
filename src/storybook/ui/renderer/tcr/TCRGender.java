/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.renderer.tcr;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import storybook.model.hbn.entity.Gender;
import storybook.tools.LOG;

@SuppressWarnings("serial")
public class TCRGender extends DefaultTableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel lb = (JLabel) super.getTableCellRendererComponent(table, null,
				isSelected, hasFocus, row, column);
		if (value == null) {
			lb.setText("");
		} else if (value instanceof String) {
			lb.setText(value.toString());
		} else if (value instanceof Gender) {
			try {
				Gender gender = (Gender) value;
				lb.setText(gender.toString());
				lb.setToolTipText(gender.toString());
				lb.setIcon(gender.getIcon());
			} catch (Exception e) {
				LOG.err("GenderTCR error", e);
			}
		}
		return lb;
	}
}
