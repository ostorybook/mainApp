/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.dialog;

import i18n.I18N;
import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import org.hibernate.Session;
import api.mig.swing.MigLayout;
import resources.icons.ICONS;
import resources.icons.IconUtil;
import storybook.model.Model;
import storybook.model.book.Book;
import storybook.model.hbn.dao.ChapterDAO;
import storybook.model.hbn.dao.IdeaDAO;
import storybook.model.hbn.dao.ItemDAO;
import storybook.model.hbn.dao.LocationDAO;
import storybook.model.hbn.dao.MemoDAO;
import storybook.model.hbn.dao.PartDAO;
import storybook.model.hbn.dao.PersonDAO;
import storybook.model.hbn.dao.PlotDAO;
import storybook.model.hbn.dao.SceneDAO;
import storybook.model.hbn.dao.StrandDAO;
import storybook.model.hbn.dao.TagDAO;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.hbn.entity.Attribute;
import storybook.model.hbn.entity.Category;
import storybook.model.hbn.entity.Chapter;
import storybook.model.hbn.entity.Event;
import storybook.model.hbn.entity.Gender;
import storybook.model.hbn.entity.Idea;
import storybook.model.hbn.entity.Item;
import storybook.model.hbn.entity.Itemlink;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Memo;
import storybook.model.hbn.entity.Part;
import storybook.model.hbn.entity.Person;
import storybook.model.hbn.entity.Plot;
import storybook.model.hbn.entity.Relationship;
import storybook.model.hbn.entity.Scene;
import storybook.model.hbn.entity.Strand;
import storybook.model.hbn.entity.Tag;
import storybook.model.hbn.entity.Taglink;
import storybook.tools.TextUtil;
import storybook.tools.html.Html;
import storybook.tools.swing.FontUtil;
import storybook.tools.swing.SwingUtil;
import storybook.tools.swing.js.JSLabel;
import storybook.ui.MIG;
import storybook.ui.MainFrame;
import static storybook.ui.Ui.MINIMUM_SIZE;

/**
 *
 * @author favdb
 */
public class ReplaceDlg extends AbstractDialog {

	private String words;
	private final JPanel resultat = new JPanel();
	private JTextField txWords;
	List<JCheckBox> cbList;
	String[] objects = {"strand", "part", "chapter", "scene", "person", "plot", "location", "item", "tag", "idea", "memo"};
	private JButton btAll;
	private JTextField txByWords;
	private String bywords;
	private JButton btReplace;

	public static void show(MainFrame m) {
		SwingUtil.showModalDialog(new ReplaceDlg(m), m, true);
	}

	public ReplaceDlg(MainFrame m) {
		super(m);
		initAll();
	}

	@Override
	public void init() {
		// empty
	}

	@Override
	public void initUi() {
		JSLabel lb1 = new JSLabel(I18N.getMsg("search.words"));
		txWords = new JTextField(32);
		txWords.addCaretListener((CaretEvent evt) -> {
			checkIfOk();
		});
		JSLabel lb2 = new JSLabel(I18N.getMsg("replace.bywords"));
		txByWords = new JTextField(32);
		txByWords.addCaretListener((CaretEvent evt) -> {
			checkIfOk();
		});
		btAll = new JButton(I18N.getMsg("all"));
		btAll.addActionListener((ActionEvent evt) -> {
			selectAll();
		});
		JPanel jPanel1 = new JPanel(new MigLayout("wrap 4"));
		jPanel1.setBorder(BorderFactory.createTitledBorder(I18N.getMsg("search.for")));
		cbList = new ArrayList<>();
		for (String str : objects) {
			JCheckBox cb = new JCheckBox(I18N.getMsg(str));
			cb.setName(str);
			cb.addActionListener((ActionEvent evt) -> {
				checkIfOk();
			});
			jPanel1.add(cb);
			cbList.add(cb);
		}
		jPanel1.add(new JSLabel(""), MIG.WRAP);
		jPanel1.add(btAll, MIG.get(MIG.SPAN, MIG.RIGHT));

		btReplace = new JButton(I18N.getMsg("find"));
		btReplace.setIcon(IconUtil.getIconSmall(ICONS.K.SEARCH));
		btReplace.setEnabled(false);
		btReplace.addActionListener((ActionEvent evt) -> {
			searchEntities();
		});

		//layout
		setLayout(new MigLayout());
		setTitle(I18N.getMsg("search"));
		add(lb1, MIG.WRAP);
		add(txWords, MIG.get(MIG.CENTER, MIG.WRAP));
		add(lb2, MIG.WRAP);
		add(txByWords, MIG.get(MIG.CENTER, MIG.WRAP));
		add(jPanel1, MIG.get(MIG.SPAN, MIG.WRAP));
		add(getCancelButton(), MIG.get(MIG.SG, MIG.SPAN, MIG.SPLIT2, MIG.RIGHT));
		add(btReplace, MIG.SG);
		pack();
		setLocationRelativeTo(mainFrame);
		this.setModal(true);
	}

	private void checkIfOk() {
		boolean b = !(txWords.getText().isEmpty() && txByWords.getText().isEmpty());
		boolean bb = false;
		for (JCheckBox cb : cbList) {
			if (cb.isSelected()) {
				bb = true;
				break;
			}
		}
		if (bb == false) {
			b = false;
		}
		btReplace.setEnabled(b);
	}

	private void selectAll() {
		boolean x;
		if (btAll.getText().equals(I18N.getMsg("all"))) {
			btAll.setText(I18N.getMsg("none"));
			x = true;
		} else {
			btAll.setText(I18N.getMsg("all"));
			x = false;
		}
		for (JCheckBox cb : cbList) {
			cb.setSelected(x);
		}
		checkIfOk();
	}

	private void searchEntities() {
		String s = Html.htmlToText(txWords.getText());
		if (s.isEmpty()) {
			return;
		}
		words = s;
		initResultat();
		int x = 0;
		for (JCheckBox cb : cbList) {
			if (cb.isSelected()) {
				int y = 0;
				switch (Book.getTYPE(cb.getName())) {
					case STRAND:
						y += findStrands();
						break;
					case PART:
						y += findParts();
						break;
					case CHAPTER:
						y += findChapters();
						break;
					case SCENE:
						y += findScenes();
						break;
					case PERSON:
						y += findPersons();
						break;
					case PLOT:
						y += findPlots();
						break;
					case LOCATION:
						y += findLocations();
						break;
					case ITEM:
						y += findItems();
						break;
					case TAG:
						y += findTags();
						break;
					case IDEA:
						y += findIdeas();
						break;
					case MEMO:
						y += findMemos();
						break;
					default:
						y = -1;
						break;
				}
				if (y == -1) {
					x = y;
					break;
				}
				if (y > 0) {
					x += y;
				}
			}
		}
		if (x < 0) {
			SwingUtil.showError("search.error.object");
			return;
		}
		if (x > 0) {
			JButton bt = new JButton(I18N.getMsg("replace.all"));
			bt.addActionListener((ActionEvent evt) -> {
				replaceAll("all");
			});
			resultat.add(bt, MIG.SPAN);
		}
		showResults(resultat);
	}

	private int findStrands() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		StrandDAO dao = new StrandDAO(session);
		List<Strand> entities = dao.findAll();
		int finds = 0;
		for (Strand entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				finds++;
			}
		}
		doTitle("strand", finds);
		for (Strand entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				if (finds == 0) {
					doNext();
				}
				doEntity("strand", entity.getName(), entity);
			}
		}
		return (finds);
	}

	private int findParts() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PartDAO dao = new PartDAO(session);
		List<Part> entities = dao.findAll();
		int finds = 0;
		for (Part entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				finds++;
			}
		}
		doTitle("part", finds);
		for (Part entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				if (finds == 0) {
					doNext();
				}
				doEntity("prat", entity.getName(), entity);
			}
		}
		return (finds);
	}

	private int findChapters() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		ChapterDAO dao = new ChapterDAO(session);
		List<Chapter> entities = dao.findAll();
		int finds = 0;
		for (Chapter entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				finds++;
			}
		}
		doTitle("chapter", finds);
		for (Chapter entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				if (finds == 0) {
					doNext();
				}
				doEntity("chapter", entity.getChapternoStr() + " " + entity.getName(), entity);
			}
		}
		return (finds);
	}

	private int findScenes() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		SceneDAO dao = new SceneDAO(session);
		List<Scene> entities = dao.findAll();
		int finds = 0;
		for (Scene entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))
					|| searchWordsHtml(entity.getSummary())) {
				finds++;
			}
		}
		doTitle("scene", finds);
		for (Scene entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))
					|| searchWordsHtml(entity.getSummary())) {
				if (finds == 0) {
					doNext();
				}
				doEntity("scene", entity.getFullTitle(), entity);
			}
		}
		return (finds);
	}

	private int findPersons() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PersonDAO dao = new PersonDAO(session);
		List<Person> entities = dao.findAll();
		int finds = 0;
		for (Person entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				finds++;
			}
		}
		doTitle("person", finds);
		for (Person entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				if (finds == 0) {
					doNext();
				}
				doEntity("person", entity.getFullNameAbbr(), entity);
			}
		}
		return (finds);
	}

	private int findLocations() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		LocationDAO dao = new LocationDAO(session);
		List<Location> entities = dao.findAll();
		int finds = 0;
		for (Location entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				finds++;
			}
		}
		doTitle("location", finds);
		for (Location entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				if (finds == 0) {
					doNext();
				}
				doEntity("location", entity.getName(), entity);
			}
		}
		return (finds);
	}

	private int findItems() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		ItemDAO dao = new ItemDAO(session);
		List<Item> entities = dao.findAll();
		int finds = 0;
		for (Item entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				finds++;
			}
		}
		doTitle("item", finds);
		for (Item entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				if (finds == 0) {
					doNext();
				}
				doEntity("item", entity.getName(), entity);
			}
		}
		return (finds);
	}

	private int findPlots() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		PlotDAO dao = new PlotDAO(session);
		List<Plot> entities = dao.findAll();
		int finds = 0;
		for (Plot entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				finds++;
			}
		}
		doTitle("plot", finds);
		for (Plot entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				if (finds == 0) {
					doNext();
				}
				doEntity("plot", entity.getName(), entity);
			}
		}
		return (finds);
	}

	private int findTags() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		TagDAO dao = new TagDAO(session);
		List<Tag> entities = dao.findAll();
		int finds = 0;
		for (Tag entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				finds++;
			}
		}
		doTitle("tag", finds);
		for (Tag entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				if (finds == 0) {
					doNext();
				}
				doEntity("tag", entity.getName(), entity);
			}
		}
		return (finds);
	}

	private int findIdeas() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		IdeaDAO dao = new IdeaDAO(session);
		List<Idea> entities = dao.findAll();
		int finds = 0;
		for (Idea entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				finds++;
			}
		}
		doTitle("idea", finds);
		for (Idea entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				if (finds == 0) {
					doNext();
				}
				doEntity("idea", entity.getId() + " " + TextUtil.ellipsize(entity.getNotes(), 30), entity);
			}
		}
		return (finds);
	}

	private int findMemos() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		MemoDAO dao = new MemoDAO(session);
		List<Memo> entities = dao.findAll();
		int finds = 0;
		for (Memo entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				finds++;
			}
		}
		doTitle("memo", finds);
		for (Memo entity : entities) {
			if (searchWords(entity.toCsv(" ", " ", "\t"))) {
				if (finds == 0) {
					doNext();
				}
				doEntity("memo", entity.getName(), entity);
			}
		}
		return (finds);
	}

	private boolean searchWords(String str) {
		String r = Html.htmlToText(str);
		if (r.toLowerCase(Locale.getDefault()).contains(words.toLowerCase(Locale.getDefault()))) {
			return (true);
		}
		return (false);
	}

	private boolean searchWordsHtml(String str) {
		String r = str;
		if (r.contains(words)) {
			return (true);
		}
		return (false);
	}

	private void showResults(JPanel res) {
		ReplaceResultsDlg dlg = new ReplaceResultsDlg(mainFrame, res, words, bywords);
		dlg.setVisible(true);
	}

	private void initResultat() {
		resultat.setLayout(new MigLayout(MIG.get(MIG.TOP, MIG.WRAP), "[][]"));
		resultat.setBackground(Color.white);
		resultat.setMinimumSize(MINIMUM_SIZE);
		resultat.removeAll();
	}

	private void doTitle(String msg, int finds) {
		JSLabel lb = new JSLabel(I18N.getMsg(msg));
		lb.setFont(FontUtil.getBold());
		resultat.add(lb);
		if (finds == 0) {
			doEmpty();
		} else {
			JButton bt = new JButton(I18N.getMsg("replace.all") + " " + I18N.getMsg(msg));
			bt.setName("btReplaceAll_" + msg);
			bt.addActionListener((ActionEvent evt) -> {
				replaceAll(msg);
			});
			resultat.add(bt, MIG.SPAN);
		}
	}

	private void doEmpty() {
		JSLabel r = new JSLabel(I18N.getMsg("search.empty"));
		resultat.add(r, MIG.WRAP);
	}

	private void doEntity(String nature, String str, AbstractEntity entity) {
		resultat.add(new JSLabel(" "), MIG.RIGHT);
		JSLabel r = new JSLabel(str);
		JSLabel doOk = new JSLabel(IconUtil.getIconSmall(ICONS.K.OK));
		doOk.setVisible(false);
		JButton bt = new JButton(IconUtil.getIconSmall(ICONS.K.RENAME));
		bt.setName("btReplace_" + nature);
		bt.setToolTipText(I18N.getMsg("replace"));
		bt.addActionListener((ActionEvent evt) -> {
			replace(entity, bt, doOk);
		});
		bt.setMargin(new Insets(0, 0, 0, 0));
		resultat.add(r, MIG.get("split 3", MIG.GROWX));
		resultat.add(doOk);
		resultat.add(bt, MIG.WRAP);
	}

	private void doNext() {
		JSLabel r = new JSLabel(" ");
		resultat.add(r, MIG.WRAP);
	}

	private void replace(AbstractEntity entity, JButton bt, JSLabel doOk) {
		words = txWords.getText();
		bywords = txByWords.getText();
		switch (Book.getTYPE(entity)) {
			case ATTRIBUTE:
				replaceAttribute((Attribute) entity);
				break;
			case CATEGORY:
				replaceCategory((Category) entity);
				break;
			case CHAPTER:
				replaceChapter((Chapter) entity);
				break;
			case EVENT:
				replaceEvent((Event) entity);
				break;
			case GENDER:
				replaceGender((Gender) entity);
				break;
			case IDEA:
				replaceIdea((Idea) entity);
				break;
			case ITEM:
				replaceItem((Item) entity);
				break;
			case ITEMLINK:
				replaceItemlink((Itemlink) entity);
				break;
			case LOCATION:
				replaceLocation((Location) entity);
				break;
			case MEMO:
				replaceMemo((Memo) entity);
				break;
			case PART:
				replacePart((Part) entity);
				break;
			case PERSON:
				replacePerson((Person) entity);
				break;
			case PLOT:
				replacePlot((Plot) entity);
				break;
			case RELATION:
				replaceRelation((Relationship) entity);
				break;
			case SCENE:
				replaceScene((Scene) entity);
				break;
			case STRAND:
				replaceStrand((Strand) entity);
				break;
			case TAG:
				replaceTag((Tag) entity);
				break;
			case TAGLINK:
				replaceTaglink((Taglink) entity);
				break;
			default:
				break;
		}
		bt.setVisible(false);
		doOk.setVisible(true);
	}

	private void replaceAll(String name) {
		JButton bt = null;
		boolean all = (name.equals("all"));
		for (Component c : resultat.getComponents()) {
			if (c instanceof JButton) {
				if (all && (c.getName() != null) && c.getName().contains("btReplaceAll_")) {
					bt = (JButton) c;
				} else if ((c.getName() != null) && c.getName().contains("btReplaceAll_" + name)) {
					bt = (JButton) c;
				}
				boolean d = false;
				if (all && (c.getName() != null) && c.getName().contains("btReplace_")) {
					d = true;
				}
				if ((c.getName() != null) && c.getName().contains("btReplace_" + name)) {
					d = true;
				}
				if (d) {
					((JButton) c).doClick();
					if (bt != null) {
						bt.setVisible(false);
					}
				}
			}
		}
	}

	private void replaceEntity(AbstractEntity entity) {
		boolean b = false;
		if (entity.getDescription().contains(words)) {
			b = true;
			entity.setDescription(entity.getDescription().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replaceAttribute(Attribute entity) {
		replaceEntity(entity);
	}

	private void replaceCategory(Category entity) {
		replaceEntity(entity);
	}

	private void replaceChapter(Chapter entity) {
		boolean b = false;
		if (entity.getName().contains(words)) {
			b = true;
			entity.setTitle(entity.getName().replace(words, bywords));
		}
		if (entity.getDescription().contains(words)) {
			b = true;
			entity.setDescription(entity.getDescription().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replaceEvent(Event entity) {
		replaceEntity(entity);
	}

	private void replaceGender(Gender entity) {
		replaceEntity(entity);
	}

	private void replaceIdea(Idea entity) {
		boolean b = false;
		if (entity.getCategory().contains(words)) {
			b = true;
			entity.setCategory(entity.getCategory().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replaceItem(Item entity) {
		boolean b = false;
		if (entity.getName().contains(words)) {
			b = true;
			entity.setName(entity.getName().replace(words, bywords));
		}
		if (entity.getCategory().contains(words)) {
			b = true;
			entity.setCategory(entity.getCategory().replace(words, bywords));
		}
		if (entity.getDescription().contains(words)) {
			b = true;
			entity.setDescription(entity.getDescription().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replaceItemlink(Itemlink entity) {
		replaceEntity(entity);
	}

	private void replaceLocation(Location entity) {
		boolean b = false;
		if (entity.getName().contains(words)) {
			b = true;
			entity.setName(entity.getName().replace(words, bywords));
		}
		if (entity.getAddress().contains(words)) {
			b = true;
			entity.setAddress(entity.getAddress().replace(words, bywords));
		}
		if (entity.getCity().contains(words)) {
			b = true;
			entity.setCity(entity.getCity().replace(words, bywords));
		}
		if (entity.getCountry().contains(words)) {
			b = true;
			entity.setCountry(entity.getCountry().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replaceMemo(Memo entity) {
		boolean b = false;
		if (entity.getName().contains(words)) {
			b = true;
			entity.setName(entity.getName().replace(words, bywords));
		}
		if (entity.getCategory().contains(words)) {
			b = true;
			entity.setCategory(entity.getCategory().replace(words, bywords));
		}
		if (entity.getDescription().contains(words)) {
			b = true;
			entity.setDescription(entity.getDescription().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replacePart(Part entity) {
		boolean b = false;
		if (entity.getName().contains(words)) {
			b = true;
			entity.setName(entity.getName().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replacePerson(Person entity) {
		boolean b = false;
		if (entity.getFirstname().contains(words)) {
			b = true;
			entity.setFirstname(entity.getFirstname().replace(words, bywords));
		}
		if (entity.getLastname().contains(words)) {
			b = true;
			entity.setLastname(entity.getLastname().replace(words, bywords));
		}
		if (entity.getOccupation().contains(words)) {
			b = true;
			entity.setOccupation(entity.getOccupation().replace(words, bywords));
		}
		if (entity.getDescription().contains(words)) {
			b = true;
			entity.setDescription(entity.getDescription().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replacePlot(Plot entity) {
		boolean b = false;
		if (entity.getName().contains(words)) {
			b = true;
			entity.setName(entity.getName().replace(words, bywords));
		}
		if (entity.getDescription().contains(words)) {
			b = true;
			entity.setDescription(entity.getDescription().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replaceRelation(Relationship entity) {
		replaceEntity(entity);
	}

	private void replaceScene(Scene entity) {
		boolean b = false;
		if (entity.getTitle().contains(words)) {
			b = true;
			entity.setTitle(entity.getTitle().replace(words, bywords));
		}
		if (entity.getSummary().contains(words)) {
			b = true;
			String x = entity.getSummary();
			String y = x.replace(words, bywords);
			entity.setSummary(y);
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replaceStrand(Strand entity) {
		boolean b = false;
		if (entity.getName().contains(words)) {
			b = true;
			entity.setName(entity.getName().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replaceTag(Tag entity) {
		boolean b = false;
		if (entity.getName().contains(words)) {
			b = true;
			entity.setName(entity.getName().replace(words, bywords));
		}
		if (entity.getCategory().contains(words)) {
			b = true;
			entity.setCategory(entity.getCategory().replace(words, bywords));
		}
		if (entity.getDescription().contains(words)) {
			b = true;
			entity.setDescription(entity.getDescription().replace(words, bywords));
		}
		if (entity.getNotes().contains(words)) {
			b = true;
			entity.setNotes(entity.getNotes().replace(words, bywords));
		}
		if (b) {
			mainFrame.getBookController().updateEntity(entity);
		}
	}

	private void replaceTaglink(Taglink entity) {
		replaceEntity(entity);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// empty
	}

}
