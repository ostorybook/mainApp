/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.dialog;

import api.mig.swing.MigLayout;
import i18n.I18N;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JScrollPane;
import org.hibernate.Session;
import resources.icons.ICONS;
import storybook.edit.Editor;
import storybook.model.Model;
import storybook.model.hbn.dao.ChapterDAO;
import storybook.model.hbn.entity.Chapter;
import storybook.tools.swing.SwingUtil;
import storybook.ui.MainFrame;
import storybook.ui.panel.typist.TypistPanel;

/**
 *
 * @author favdb
 */
public class ChapterSelectDlg extends JDialog {

	private JList lstChapters;
	private final Chapter chapter;
	private final MainFrame mainFrame;
	private boolean canceled = false;

	public ChapterSelectDlg(TypistPanel tp, Chapter c) {
		super(tp.getMainFrame().getFullFrame());
		mainFrame = tp.getMainFrame();
		chapter = c;
		initiatlize();
	}

	@SuppressWarnings("unchecked")
	public void initiatlize() {
		setModal(true);
		JScrollPane scroller = new JScrollPane();
		scroller.setPreferredSize(new Dimension(642, 480));
		lstChapters = new JList();
		lstChapters.setModel(new DefaultListModel());
		loadList(-1);
		lstChapters.setSelectedValue(chapter, true);
		scroller.setViewportView(lstChapters);
		JButton btNew = SwingUtil.createButton("", ICONS.K.NEW_CHAPTER,
		   "new.chapter", evt -> newChapter());
		JButton btOK = SwingUtil.createButton("ok", ICONS.K.OK,
		   "ok", evt -> dispose());
		JButton btCancel = SwingUtil.createButton("cancel", ICONS.K.CANCEL,
		   "cancel", evt -> {
			   canceled = true;
			   dispose();
		   });
		//layout
		setLayout(new MigLayout());
		setTitle(I18N.getMsg("chapter"));
		add(scroller, "split 2");
		add(btNew, "top,wrap");
		add(btCancel, "split 2,right");
		add(btOK);
		pack();
		setLocation(MouseInfo.getPointerInfo().getLocation());
	}

	@SuppressWarnings("unchecked")
	private void loadList(int first) {
		DefaultListModel listModel = (DefaultListModel) lstChapters.getModel();
		listModel.removeAllElements();
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		ChapterDAO ChapterDAO = new ChapterDAO(session);
		List<Chapter> chapters = ChapterDAO.findAll();
		for (Chapter c : chapters) {
			listModel.addElement(c);
		}
		if (first != -1) {
			lstChapters.setSelectedIndex(first);
		}
		model.commit();
	}

	private void newChapter() {
		JDialog dlg = new JDialog(this, true);
		Editor editor = new Editor(mainFrame, new Chapter(), dlg);
		dlg.setTitle(I18N.getMsg("editor"));
		Dimension pos = SwingUtil.getDlgPosition(new Chapter());
		dlg.add(editor);
		Dimension size = SwingUtil.getDlgSize(new Chapter());
		if (size != null) {
			dlg.setSize(size.height, size.width);
		} else {
			dlg.setLocationRelativeTo(this);
		}
		if (pos != null) {
			dlg.setLocation(pos.height, pos.width);
		} else {
			dlg.setSize(this.getWidth() / 2, 680);
		}
		dlg.setVisible(true);
		SwingUtil.saveDlgPosition(dlg, new Chapter());
		loadList(-1);
		lstChapters.setSelectedValue(chapter, true);
	}

	public Chapter getSelectedChapter() {
		//LOG.trace("initialChapter=" + chapter.toString());
		//LOG.trace("selectedChapterIndex=" + lstChapters.getSelectedIndex());
		//LOG.trace("selectedChapter=" + ((Chapter) lstChapters.getSelectedValue()).toString());
		return ((Chapter) lstChapters.getSelectedValue());
	}

	public boolean isCanceled() {
		return (canceled);
	}

}
