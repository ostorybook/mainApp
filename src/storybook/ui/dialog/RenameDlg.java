/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.ui.dialog;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import org.hibernate.Session;
import api.mig.swing.MigLayout;
import storybook.ctrl.Ctrl;
import i18n.I18N;
import storybook.model.Model;
import storybook.model.hbn.dao.ItemDAO;
import storybook.model.hbn.dao.LocationDAO;
import storybook.model.hbn.dao.TagDAO;
import storybook.model.hbn.entity.Item;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Tag;
import storybook.tools.swing.js.JSLabel;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class RenameDlg extends AbstractDialog {

	private final String which;
	JComboBox<String> combo;
	JTextField tfNewName;

	public RenameDlg(MainFrame parent, String which) {
		super(parent);
		this.which = which;
		initAll();
	}

	@Override
	public void init() {
		//empty
	}

	@Override
	public void initUi() {
		JSLabel lbRename = new JSLabel(I18N.getMsg("rename.rename"));
		combo = new JComboBox<>();
		JSLabel lbTo = new JSLabel(I18N.getMsg("rename.to"));
		tfNewName = new JTextField(20);
		switch (which) {
			case "country":
				this.setTitle(I18N.getMsg("location.rename.country"));
				createListCombo(countryGetList());
				break;
			case "city":
				this.setTitle(I18N.getMsg("location.rename.city"));
				createListCombo(cityGetList());
				break;
			case "item":
				this.setTitle(I18N.getMsg("item.rename.category"));
				createListCombo(itemCategoryGetList());
				break;
			case "tag":
				this.setTitle(I18N.getMsg("tag.rename.category"));
				createListCombo(tagCategoryGetList());
				break;
			default:
				break;
		}
		//layout
		setLayout(new MigLayout("wrap 4", "[]", "[]20[]"));
		setTitle(I18N.getMsg("rename"));
		add(lbRename);
		add(combo);
		add(lbTo);
		add(tfNewName);
		add(getOkButton(), "sg,span,split 2,right");
		add(getCancelButton(), "sg");
		pack();
		setLocationRelativeTo(mainFrame);
		this.setModal(true);
	}

	@Override
	protected AbstractAction getOkAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String oldValue = (String) combo.getSelectedItem();
				String newValue = tfNewName.getText();
				if (!newValue.isEmpty()) {
					rename(oldValue, newValue);
					dispose();
				}
			}
		};
	}

	@SuppressWarnings("unchecked")
	private void createListCombo(List<String> list) {
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		for (String category : list) {
			model.addElement(category);
		}
		combo.setModel(model);
	}

	private void rename(String oldValue, String newValue) {
		switch (which) {
			case "country":
				countryRename(oldValue, newValue);
				break;
			case "city":
				cityRename(oldValue, newValue);
				break;
			case "item":
				itemCategoryRename(oldValue, newValue);
				break;
			case "tag":
				tagCategoryRename(oldValue, newValue);
				break;
			default:
				break;
		}
	}

	protected List<String> countryGetList() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		LocationDAO dao = new LocationDAO(session);
		List<String> ret = dao.findCountries();
		model.commit();
		return ret;
	}

	protected void countryRename(String oldValue, String newValue) {
		Model model = mainFrame.getBookModel();
		Ctrl ctrl = mainFrame.getBookController();
		Session session = model.beginTransaction();
		LocationDAO dao = new LocationDAO(session);
		List<Location> locations = dao.findByCountry(oldValue);
		model.commit();
		for (Location location : locations) {
			location.setCountry(newValue);
			ctrl.updateEntity(location);
		}
	}

	protected List<String> cityGetList() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		LocationDAO dao = new LocationDAO(session);
		List<String> ret = dao.findCities();
		model.commit();
		return ret;
	}

	protected void cityRename(String oldValue, String newValue) {
		Model model = mainFrame.getBookModel();
		Ctrl ctrl = mainFrame.getBookController();
		Session session = model.beginTransaction();
		LocationDAO dao = new LocationDAO(session);
		List<Location> locations = dao.findByCity(oldValue);
		model.commit();
		for (Location location : locations) {
			location.setCity(newValue);
			ctrl.updateEntity(location);
		}
	}

	protected List<String> itemCategoryGetList() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		ItemDAO dao = new ItemDAO(session);
		List<String> ret = dao.findCategories();
		model.commit();
		return ret;
	}

	protected void itemCategoryRename(String oldValue, String newValue) {
		Model model = mainFrame.getBookModel();
		Ctrl ctrl = mainFrame.getBookController();
		Session session = model.beginTransaction();
		ItemDAO dao = new ItemDAO(session);
		List<Item> items = dao.findByCategory(oldValue);
		model.commit();
		for (Item item : items) {
			item.setCategory(newValue);
			ctrl.updateEntity(item);
		}
	}

	protected List<String> tagCategoryGetList() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		TagDAO dao = new TagDAO(session);
		List<String> ret = dao.findCategories();
		model.commit();
		return ret;
	}

	protected void tagCategoryRename(String oldValue, String newValue) {
		Model model = mainFrame.getBookModel();
		Ctrl ctrl = mainFrame.getBookController();
		Session session = model.beginTransaction();
		TagDAO dao = new TagDAO(session);
		List<Tag> tags = dao.findByCategory(oldValue);
		model.commit();
		for (Tag tag : tags) {
			tag.setCategory(newValue);
			ctrl.updateEntity(tag);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// empty
	}

}
