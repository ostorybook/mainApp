/*
 * Copyright (C) 2023 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.tools.xml;

/**
 * class for the XMLkey used
 *
 * @author favdb
 */
public class XmlKey {

	public enum XK {
		ID,
		ABBREVIATION,
		ADDRESS,
		ALTITUDE,
		ASPECT,
		ASSISTANT,
		ATTRIBUT,
		BIRTHDAY, DEATH, OCCUPATION,
		CATEGORY, CATEGORIES,
		CHAPTER,
		CITY,
		COLOR,
		COUNTRY,
		CREATION,
		DATE,
		DESCRIPTION,
		DURATION,
		ENDNOTE,
		EPISODE,
		EVENT,
		FILE, ICON,
		FIRSTNAME, LASTNAME,
		GENDER,
		GPS,
		IDEA,
		INFORMATIVE,
		INTENSITY,
		ITEM, ITEMS,
		KEY, VALUE,
		LOCATION, LOCATIONS,
		MAJ,
		MEMO,
		NAME,
		NARRATOR,
		NOTES,
		NUMBER,
		OBJECTIVEDATE,
		OBJECTIVEDONE,
		OBJECTIVECHARS,
		PART,
		PERSON, PERSONS,
		PLOT, PLOTS,
		RELATIVESCENE, RELATIVETIME,
		RELATION, RELATIONS,
		//scenario
		LOC, MOMENT, PITCH, START, END,
		SCENE,
		STAGE,
		STEP,
		TEXT,
		TIME,
		TYPE,
		SORT,
		STATUS,
		STRAND, STRANDS,
		SUP,
		TAG, TAGS,
		TAGLINK,
		UUID;
	}

}
