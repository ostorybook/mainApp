/*
 * Copyright (C) 2023 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package storybook.exim.exporter;

import java.io.File;
import java.util.List;
import org.hibernate.Session;
import static storybook.exim.exporter.AbstractExport.VERBOSE;
import static storybook.exim.exporter.AbstractExport.stringAttribute;
import storybook.exim.exporter.ExportTable.KW;
import storybook.model.Model;
import storybook.model.book.Book;
import storybook.model.book.BookParam;
import storybook.model.book.BookParamExport;
import storybook.model.hbn.dao.AttributeDAO;
import storybook.model.hbn.dao.CategoryDAO;
import storybook.model.hbn.dao.ChapterDAO;
import storybook.model.hbn.dao.EndnoteDAO;
import storybook.model.hbn.dao.EpisodeDAO;
import storybook.model.hbn.dao.EventDAO;
import storybook.model.hbn.dao.GenderDAO;
import storybook.model.hbn.dao.IdeaDAO;
import storybook.model.hbn.dao.ItemDAO;
import storybook.model.hbn.dao.ItemlinkDAO;
import storybook.model.hbn.dao.LocationDAO;
import storybook.model.hbn.dao.MemoDAO;
import storybook.model.hbn.dao.PartDAO;
import storybook.model.hbn.dao.PersonDAO;
import storybook.model.hbn.dao.PlotDAO;
import storybook.model.hbn.dao.RelationDAO;
import storybook.model.hbn.dao.SceneDAO;
import storybook.model.hbn.dao.StatusDAO;
import storybook.model.hbn.dao.StrandDAO;
import storybook.model.hbn.dao.TagDAO;
import storybook.model.hbn.dao.TaglinkDAO;
import storybook.model.hbn.entity.Attribute;
import storybook.model.hbn.entity.Category;
import storybook.model.hbn.entity.Chapter;
import storybook.model.hbn.entity.Endnote;
import storybook.model.hbn.entity.Episode;
import storybook.model.hbn.entity.Event;
import storybook.model.hbn.entity.Gender;
import storybook.model.hbn.entity.Idea;
import storybook.model.hbn.entity.Item;
import storybook.model.hbn.entity.Itemlink;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Memo;
import storybook.model.hbn.entity.Part;
import storybook.model.hbn.entity.Person;
import storybook.model.hbn.entity.Plot;
import storybook.model.hbn.entity.Relationship;
import storybook.model.hbn.entity.Scene;
import storybook.model.hbn.entity.Status;
import storybook.model.hbn.entity.Strand;
import storybook.model.hbn.entity.Tag;
import storybook.model.hbn.entity.Taglink;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class ExportDB {

	public static void exec(MainFrame mainFrame) {
		ExportDB exp = new ExportDB(mainFrame);
		exp.doExec();
	}

	private final MainFrame mainFrame;
	private final BookParamExport exportParam;
	public BookParam bookParam;
	private ExportTable export;
	private final Book book;

	public ExportDB(MainFrame m) {
		this.mainFrame = m;
		this.exportParam = mainFrame.getBook().getParam().getParamExport();
		this.book = mainFrame.getBook();
		init();
	}

	private void init() {
		// if export directory is empty then set it to default
		boolean b = false;
		if (exportParam.getDirectory().isEmpty()) {
			b = true;
		} else {
			File dir = new File(exportParam.getDirectory());
			if (!dir.exists() || !dir.isDirectory()) {
				b = true;
			}
		}
		if (b) {
			exportParam.setDirectory(mainFrame.getH2File().getPath());
			exportParam.save();
		}
	}

	public void doExec() {
		export = new ExportTable(mainFrame, KW.XML.toString());
		if (!export.openFile(mainFrame.getH2File().getName(), false)) {
			return;
		}
		writeInfo();
		writeParam();
		writeAttributes();
		writeCategories();
		writeGenders();
		writeStatuss();
		writeIdeas();
		writeStrands();
		writeEndnotes();
		writeEpisodes();
		writeEvents();
		writeItems();
		//writeItemlinks();
		writeLocations();
		writeMemos();
		writePersons();
		writePlots();
		writeRelations();
		writeTags();
		writeParts();
		writeChapters();
		writeScenes();
		//writeTaglinks();
		export.closeFile(VERBOSE);
	}

	private void writeInfo() {
		export.writeText("    <info ");
		StringBuilder b = new StringBuilder();
		b.append(stringAttribute(0, KW.TITLE.toString(), book.getTitle()));
		b.append(stringAttribute(12, "subtitle", book.getSubtitle()));
		b.append(stringAttribute(12, "author", book.getAuthor()));
		b.append(stringAttribute(12, "copyright", book.getCopyright()));
		b.append(stringAttribute(12, "uuid", book.getUUID()));
		b.append(stringAttribute(12, "lang", book.getLanguage()));
		b.append(stringAttribute(12, "review", book.getReview() ? "1" : "0"));
		b.append(stringAttribute(12, "scenario", book.getScenario() ? "1" : "0"));
		b.append(stringAttribute(12, "markdown", book.getMarkdown() ? "1" : "0"));
		b.append(stringAttribute(12, "nature", book.getInteger(Book.INFO.NATURE) + ""));
		b.append(stringAttribute(12, "dedication", book.getDedication() + ""));
		export.writeText(b.toString() + ">\n");
		//todo check if assistant XML is ok
		String v = book.info.assistantGet();
		if (!v.isEmpty()) {
			export.writeXml(2, KW.ASSISTANT.toString(), v);
		}
		//blurb
		v = book.getBlurb();
		if (!v.isEmpty()) {
			export.writeXml(2, KW.BLURB.toString(), v);
		}
		//notes
		v = book.getNotes();
		if (!v.isEmpty()) {
			export.writeXml(2, KW.NOTES.toString(), v);
		}
		export.writeXml(1, "/info", "");
	}

	private void writeParam() {
		export.writeXml(1, KW.PARAM.toString(), "");
		export.writeXmlAttribute(2, KW.BACKUP.toString(), book.param.getParamBackup().toXml());
		export.writeXmlAttribute(2, KW.EDITOR.toString(), book.param.getParamEditor().toXml());
		export.writeXmlAttribute(2, KW.EXPORT.toString(), book.param.getParamExport().toXml());
		export.writeXmlAttribute(2, KW.IMPORT.toString(), book.param.getParamImport().toXml());
		export.writeXmlAttribute(2, KW.LAYOUT.toString(), book.param.getParamLayout().toXml());
		export.writeXml(1, "/" + KW.PARAM.toString(), "");
	}

	private void writeAttributes() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		AttributeDAO dao = new AttributeDAO(session);
		List<Attribute> entities = dao.findAll();
		for (Attribute e : entities) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeCategories() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		CategoryDAO dao = new CategoryDAO(session);
		List<Category> entities = dao.findAll();
		for (Category e : entities) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeChapters() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		ChapterDAO dao = new ChapterDAO(session);
		List<Chapter> entities = dao.findAll();
		for (Chapter e : entities) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeEndnotes() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		EndnoteDAO dao = new EndnoteDAO(session);
		List<Endnote> entities = dao.findAll();
		for (Endnote e : entities) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeEpisodes() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		EpisodeDAO dao = new EpisodeDAO(session);
		List<Episode> entities = dao.findAll();
		for (Episode e : entities) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeEvents() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		EventDAO dao = new EventDAO(session);
		List<Event> entities = dao.findAll();
		for (Event e : entities) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeGenders() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		GenderDAO dao = new GenderDAO(session);
		List<Gender> entities = dao.findAll();
		for (Gender e : entities) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeIdeas() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Idea e : new IdeaDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeItems() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Item e : new ItemDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeItemlinks() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Itemlink e : new ItemlinkDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeLocations() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		LocationDAO dao = new LocationDAO(session);
		List<Location> entities = dao.findAll();
		for (Location e : entities) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeMemos() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Memo e : new MemoDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeParts() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Part e : new PartDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writePersons() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Person e : new PersonDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writePlots() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Plot e : new PlotDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeRelations() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Relationship e : new RelationDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeScenes() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Scene e : new SceneDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeStatuss() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Status e : new StatusDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeStrands() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Strand e : new StrandDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeTags() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Tag e : new TagDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

	private void writeTaglinks() {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		for (Taglink e : new TaglinkDAO(session).findAll()) {
			export.writeText(e.toXml());
		}
		model.commit();
	}

}
