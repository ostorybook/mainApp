/*
 * Copyright (C) 2023 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model;

import api.mig.swing.MigLayout;
import i18n.I18N;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import static storybook.ctrl.Ctrl.PROPS.DELETE;
import static storybook.ctrl.Ctrl.PROPS.NEW;
import static storybook.ctrl.Ctrl.PROPS.UPDATE;
import storybook.model.book.Book;
import static storybook.model.book.Book.TYPE.*;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.tools.swing.FontUtil;
import storybook.ui.MIG;
import storybook.ui.SbView;
import static storybook.ui.SbView.VIEWNAME.*;

/**
 * all Fire action for the ProjModel
 *
 * @author favdb
 */
public class ProjFires {

	private final Model proj;

	public ProjFires(Model proj) {
		this.proj = proj;
	}

	/**
	 * fire all
	 */
	public void fireAgain() {
		//LOG.trace(TT + ".fireAgain()");
		initProgress();
		new Thread(new ExecThread()).start();
	}

	public void fireAgain(SbView view) {
		//LOG.trace(TT + ".fireAgain(" + view.getName() + ")");
		if (view == null || !view.isLoaded()) {
			return;
		}
		switch (SbView.getVIEW(view.getName())) {
			// views
			case BOOK:
				fireAgainScenes();
				break;
			case CHRONO:
				fireAgainScenes();
				break;
			case MANAGE:
				fireAgainChapters();
				break;
			case READING:
				fireAgainChapters();
				break;
			case STORY_CLASSIC:
			case STORY_FREYTAG:
				fireAgainChapters();
				break;
			case STORY_VOGLER:
				fireAgainChapters();
				break;
			case TIMELINE:
				fireAgainScenes();
				break;
			// tables
			case ATTRIBUTES:
				fireAgainAttributes();
				break;
			case CATEGORIES:
				fireAgainCategories();
				break;
			case CHAPTERS:
				fireAgainChapters();
				break;
			case ENDNOTES:
				fireAgainEndnotes();
				break;
			case EPISODES:
				fireAgainEpisodes();
				break;
			case EVENTS:
				fireAgainEvents();
				break;
			case GENDERS:
				fireAgainGenders();
				break;
			case IDEAS:
				fireAgainIdeas();
				break;
			case INTERNALS:
				fireAgainInternals();
			case ITEMS:
				fireAgainItems();
				break;
			case ITEMLINKS:
				fireAgainItemlinks();
				break;
			case LOCATIONS:
				fireAgainLocations();
				break;
			case MEMOS:
				fireAgainMemos();
				break;
			case PARTS:
				fireAgainParts();
				break;
			case PERSONS:
				fireAgainPersons();
				break;
			case PLOTS:
				fireAgainPlots();
				break;
			case RELATIONS:
				fireAgainRelations();
				break;
			case SCENES:
				fireAgainScenes();
				break;
			case STRANDS:
				fireAgainStrands();
				break;
			case TAGS:
				fireAgainTags();
				break;
			case TAGLINKS:
				fireAgainTaglinks();
				break;
			default:
				break;
		}
	}

	public void fireAgainReading() {
		fireAgainParts();
		fireAgainChapters();
		fireAgainScenes();
	}

	private void fireAgainAttributes() {
		//LOG.trace(TT+".fireAgainAttributes()");
		proj.firePropertyChange(ATTRIBUTE);
	}

	private void fireAgainCategories() {
		//LOG.trace(TT+".fireAgainCategories()");
		proj.firePropertyChange(CATEGORY);
	}

	public void fireAgainChapters() {
		//LOG.trace(TT+".fireAgainChapters()");
		proj.firePropertyChange(CHAPTER);
	}

	private void fireAgainEndnotes() {
		//LOG.trace(TT+".fireAgainEndnotes()");
		proj.firePropertyChange(ENDNOTE);
	}

	private void fireAgainEpisodes() {
		//LOG.trace(TT+".fireAgainEndnotes()");
		proj.firePropertyChange(EPISODE);
	}

	private void fireAgainEvents() {
		//LOG.trace(TT+".fireAgainEvents()");
		proj.firePropertyChange(EVENT);
	}

	private void fireAgainGenders() {
		//LOG.trace(TT+".fireAgainGenders()");
		proj.firePropertyChange(GENDER);
	}

	private void fireAgainIdeas() {
		//LOG.trace(TT+".fireAgainIdeas()");
		proj.firePropertyChange(IDEA);
	}

	private void fireAgainInternals() {
		//LOG.trace(TT+".fireAgainInternals()");
		proj.firePropertyChange(INTERNAL);
	}

	private void fireAgainItems() {
		//LOG.trace(TT+".fireAgainItems()");
		proj.firePropertyChange(ITEM);
	}

	private void fireAgainItemlinks() {
		//LOG.trace(TT+".fireAgainItemlinks()");
		proj.firePropertyChange(ITEMLINK);
	}

	private void fireAgainLocations() {
		//LOG.trace(TT+".fireAgainLocations()");
		proj.firePropertyChange(LOCATION);
	}

	private void fireAgainMemos() {
		//LOG.trace(TT+".fireAgainMemos()");
		proj.firePropertyChange(MEMO);
	}

	public void fireAgainParts() {
		//LOG.trace(TT+".fireAgainParts()");
		proj.firePropertyChange(PART);
	}

	private void fireAgainPersons() {
		//LOG.trace(TT+".fireAgainPersons()");
		proj.firePropertyChange(PERSON);
	}

	private void fireAgainPlots() {
		//LOG.trace(TT+".fireAgainPlots()");
		proj.firePropertyChange(PLOT);
	}

	private void fireAgainRelations() {
		//LOG.trace(TT+".fireAgainRelations()");
		proj.firePropertyChange(RELATION);
	}

	public void fireAgainScenes() {
		//LOG.trace(TT+".fireAgainScenes()");
		proj.firePropertyChange(SCENE);
	}

	private void fireAgainStrands() {
		//LOG.trace(TT+".fireAgainStrands()");
		proj.firePropertyChange(STRAND);
	}

	private void fireAgainTags() {
		//LOG.trace(TT+".fireAgainTags()");
		proj.firePropertyChange(TAG);
	}

	private void fireAgainTaglinks() {
		//LOG.trace(TT+".fireAgainTaglinks()");
		proj.firePropertyChange(TAGLINK);
	}

	/**
	 * fire Table update
	 *
	 * @param old
	 * @param entity
	 */
	public void fireTableUpdate(AbstractEntity old, AbstractEntity entity) {
		if (old == null) {
			proj.firePropertyChange(Book.getTYPE(entity), NEW, null, entity);
		} else if (entity == null) {
			proj.firePropertyChange(Book.getTYPE(old), DELETE, null, entity);
		} else {
			proj.firePropertyChange(Book.getTYPE(entity), UPDATE, old, entity);
		}
	}

	private JFrame progressFrame;
	private JProgressBar progressBar;

	Book.TYPE toRefresh[] = {
		ATTRIBUTE,
		CATEGORY,
		CHAPTER,
		ENDNOTE,
		ENDNOTE,
		EPISODE,
		EVENT,
		GENDER,
		IDEA,
		ITEM,
		ITEMLINK,
		LOCATION,
		MEMO,
		PART,
		PERSON,
		PLOT,
		RELATION,
		SCENE,
		STRAND,
		TAG,
		TAGLINK
	};

	/**
	 * initialize the progress Frame
	 */
	private void initProgress() {
		progressFrame = new JFrame();
		progressFrame.setUndecorated(true);
		progressFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel pane = (JPanel) progressFrame.getContentPane();
		pane.setLayout(new MigLayout(MIG.WRAP));
		pane.add(new JLabel(I18N.getMsg("refresh")), MIG.CENTER);
		progressBar = new JProgressBar(0, toRefresh.length);
		progressBar.setMinimumSize(new Dimension(410, FontUtil.getHeight()));
		progressBar.setStringPainted(true);
		pane.add(progressBar, MIG.GROWX);
		progressFrame.pack();
		progressFrame.setLocationRelativeTo(proj.mainFrame);
		progressFrame.setVisible(true);
	}

	public class ExecThread implements Runnable {

		public ExecThread() {
		}

		@Override
		public void run() {
			int i = 0, total = toRefresh.length;
			for (Book.TYPE type : toRefresh) {
				progressBar.setValue(i);
				proj.firePropertyChange(type);
				i++;
				progressBar.setString(i + "/" + total
				   + " (" + I18N.getMsg(type.toString()) + ")");
			}
			progressFrame.dispose();
		}
	}

}
