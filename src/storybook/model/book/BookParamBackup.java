/*
Copyright ???? by ????
Modifications ???? by ????

This file is part of oStorybook.

    oStorybook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    oStorybook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with oStorybook.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.book;

import static storybook.exim.exporter.AbstractExport.stringAttribute;

/**
 *
 * @author FaVdB
 */
public class BookParamBackup extends BookParamAbstract {

	public enum KW {
		DIRECTORY, AUTO, INCREMENT;

		@Override
		public String toString() {
			return name().toLowerCase();
		}
	}

	private String directory;
	private boolean auto;
	private int increment;

	public BookParamBackup(BookParam param) {
		super(param, "backup");
		init();
	}

	public void setDirectory(String d) {
		directory = d;
	}

	public String getDirectory() {
		return (directory);
	}

	public void setAuto(boolean d) {
		auto = d;
	}

	public boolean getAuto() {
		return (auto);
	}

	public void setIncrement(int d) {
		increment = d;
	}

	public int getIncrement() {
		return (increment);
	}

	@Override
	protected void init() {
		setDirectory(param.book.getString(Book.PARAM.BACKUP_DIR));
		setAuto(param.book.getBoolean(Book.PARAM.BACKUP_AUTO));
		setIncrement(param.book.getInteger(Book.PARAM.BACKUP_INCREMENT));
	}

	@Override
	protected void save() {
		param.book.setString(Book.PARAM.BACKUP_DIR, getDirectory());
		param.book.setBoolean(Book.PARAM.BACKUP_AUTO, getAuto());
		param.book.setInteger(Book.PARAM.BACKUP_INCREMENT, getIncrement());
	}

	@Override
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(stringAttribute(0, KW.DIRECTORY.toString(), getDirectory()));
		b.append(stringAttribute(0, KW.AUTO.toString(), getAuto() ? "1" : "0"));
		b.append(stringAttribute(0, KW.INCREMENT.toString(), getIncrement() + ""));
		return b.toString();
	}

}
