/*
 * Copyright (C) 2020 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.book;

/**
 *
 * @author favdb
 */
public abstract class BookParamAbstract {

	public String type;
	public BookParam param;

	public BookParamAbstract(BookParam param, String type) {
		this.param = param;
		this.type = type;
	}

	abstract protected void init();

	abstract protected String toXml();

	abstract protected void save();

	public int hash() {
		return toString().hashCode();
	}

	public void refresh() {
		init();
	}

}
