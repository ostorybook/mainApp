/*
Copyright ???? by ????
Modifications ???? by ????

This file is part of oStorybook.

    oStorybook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    oStorybook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with oStorybook.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.book;

import storybook.tools.calendar.SbCalendar;

/**
 *
 * @author FaVdB
 */
public class BookParamCalendar extends BookParamAbstract {

	//TODO calendar
	public final static boolean CALENDAR_DEFAULT_USE = false;
	private boolean calendarUse = false;
	private SbCalendar calendar;

	public BookParamCalendar(BookParam param) {
		super(param, "calendar");
		init();
	}

	@Override
	protected void init() {
		calendarUse = param.book.getBoolean(Book.PARAM.CALENDAR_USE);
		calendar = new SbCalendar();
		calendar.setDays(param.book.getString(Book.PARAM.CALENDAR_DAYS));
		calendar.setMonths(param.book.getString(Book.PARAM.CALENDAR_MONTHS));
		calendar.setHours(param.book.getString(Book.PARAM.CALENDAR_HOURS));
		calendar.setStartDay(param.book.getInteger(Book.PARAM.CALENDAR_STARTDAY));
	}

	@Override
	protected void save() {
		param.book.setString(Book.PARAM.CALENDAR_DAYS, calendar.getListDays());
		param.book.setString(Book.PARAM.CALENDAR_MONTHS, calendar.getListMonths());
		param.book.setString(Book.PARAM.CALENDAR_HOURS, calendar.getListHours());
		param.book.setInteger(Book.PARAM.CALENDAR_STARTDAY, calendar.getStartday());
	}

	public void setCalendarUse(boolean x) {
		calendarUse = x;
		param.book.setBoolean(Book.PARAM.CALENDAR_USE, x);
	}

	public boolean getCalendarUse() {
		return (calendarUse);
	}

	public boolean isUseCalendar() {
		return (calendarUse);
	}

	public SbCalendar getCalendar() {
		return calendar;
	}

	public void setCalendar(SbCalendar sbc) {
		calendar = sbc;
		param.book.setInteger(Book.PARAM.CALENDAR_YEARDAYS, sbc.getYearDays());
		param.book.setString(Book.PARAM.CALENDAR_HOURS, sbc.getListHours());
		param.book.setString(Book.PARAM.CALENDAR_DAYS, sbc.getListDays());
		param.book.setString(Book.PARAM.CALENDAR_MONTHS, sbc.getListMonths());
		param.book.setInteger(Book.PARAM.CALENDAR_STARTDAY, sbc.getStartday());
	}

	@Override
	public int hash() {
		return toString().hashCode();
	}

	@Override
	public void refresh() {
	}

	@Override
	public String toXml() {
		return calendar.toString();
	}

}
