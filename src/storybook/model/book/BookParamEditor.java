/*
Copyright 2018 by FaVdB
Modifications ???? by ????

This file is part of oStorybook.

    oStorybook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    oStorybook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with oStorybook.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.book;

import static storybook.exim.exporter.AbstractExport.stringAttribute;

/**
 *
 * @author FaVdB
 */
public class BookParamEditor extends BookParamAbstract {

	public enum KW {
		MODLESS, XUSE, NAME, TEMPLATE, EXT;

		@Override
		public String toString() {
			return name().toLowerCase();
		}
	}

	private boolean modless;
	private boolean use;
	private String name;
	private String extension;
	private String template;

	public BookParamEditor(BookParam param) {
		super(param, "editor");
		init();
	}

	public void setModless(boolean b) {
		modless = b;
	}

	public boolean getModless() {
		return modless;
	}

	public void setUse(boolean value) {
		use = value;
	}

	public boolean getUse() {
		return use;
	}

	public void setName(String d) {
		name = d;
	}

	public String getName() {
		return name;
	}

	public void setExtension(String d) {
		extension = d.toLowerCase();
	}

	public String getExtension() {
		return extension.toLowerCase();
	}

	public void setTemplate(String d) {
		template = d;
	}

	public String getTemplate() {
		return template;
	}

	@Override
	protected void init() {
		setModless(param.book.getBoolean(Book.PARAM.EDITOR_MODLESS));
		setUse(param.book.getBoolean(Book.PARAM.EDITOR_USE));
		setName(param.book.getString(Book.PARAM.EDITOR_NAME));
		setExtension(param.book.getString(Book.PARAM.EDITOR_EXTENSION));
		setTemplate(param.book.getString(Book.PARAM.EDITOR_TEMPLATE));
	}

	@Override
	protected void save() {
		param.book.setBoolean(Book.PARAM.EDITOR_MODLESS, getModless());
		param.book.setBoolean(Book.PARAM.EDITOR_USE, getUse());
		param.book.setString(Book.PARAM.EDITOR_NAME, getName());
		param.book.setString(Book.PARAM.EDITOR_EXTENSION, getExtension());
		param.book.setString(Book.PARAM.EDITOR_TEMPLATE, getTemplate());
	}

	@Override
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(stringAttribute(0, KW.MODLESS.toString(), getModless()));
		b.append(stringAttribute(0, KW.XUSE.toString(), getUse()));
		if (getUse()) {
			b.append(stringAttribute(0, KW.NAME.toString(), getName()));
			b.append(stringAttribute(0, KW.TEMPLATE.toString(), getTemplate()));
			b.append(stringAttribute(0, KW.EXT.toString(), getExtension()));
		}
		return b.toString();
	}

}
