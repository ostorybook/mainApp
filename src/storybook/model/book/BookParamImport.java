/**
 * Copyright 2020 by FaVdB
 *
 * This file is part of oStorybook.
 *
 *  oStorybook is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  oStorybook is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oStorybook. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package storybook.model.book;

/**
 *
 * @author FaVdB
 */
public class BookParamImport extends BookParamAbstract {

	public enum KW {
		DIRECTORY, FILE;

		@Override
		public String toString() {
			return name().toLowerCase();
		}
	}

	private String directory;
	private String file;

	public BookParamImport(BookParam param) {
		super(param, "import");
		init();
	}

	public void setDirectory(String d) {
		directory = d;
	}

	public String getDirectory() {
		return (directory);
	}

	public void setFile(String d) {
		file = d;
	}

	public String getFile() {
		return (file);
	}

	@Override
	protected void init() {
		setDirectory(param.book.getString(Book.PARAM.IMPORT_DIRECTORY));
		setFile(param.book.getString(Book.PARAM.IMPORT_FILE));
	}

	@Override
	protected void save() {
		param.book.setString(Book.PARAM.IMPORT_DIRECTORY, getDirectory());
		param.book.setString(Book.PARAM.IMPORT_FILE, getFile());
	}

	@Override
	public String toXml() {
		return "";
	}
}
