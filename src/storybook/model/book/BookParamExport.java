/*
Copyright 2018 by FaVdB
Modifications ???? by ????

This file is part of oStorybook.

    oStorybook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    oStorybook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with oStorybook.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.book;

import static storybook.exim.exporter.AbstractExport.stringAttribute;
import storybook.tools.html.Html;

/**
 *
 * @author FaVdB
 */
public class BookParamExport extends BookParamAbstract {

	private static final String TT = "BookParamExport";

	public enum KW {
		DIRECTORY, FORMAT, HTML, HIGHLIGHT, CSS, CSV, TXT;

		@Override
		public String toString() {
			return name().toLowerCase();
		}
	}

	public enum FORMAT {
		XML, HTML, OSBD, CSV, TXT, NONE;

		@Override
		public String toString() {
			return this.name().toLowerCase();
		}

		public boolean isHtml() {
			return toString().equals("html");
		}

		public boolean isXml() {
			return toString().equals("xml");
		}

		public boolean isOsbd() {
			return toString().equals("osbd");
		}

		public boolean compare(String str) {
			return this.name().toLowerCase().equals(str.toLowerCase());
		}
	}

	public static FORMAT getFormat(String str) {
		if (str != null && !str.isEmpty()) {
			for (FORMAT id : FORMAT.values()) {
				if (id.toString().equals(str)) {
					return id;
				}
			}
		}
		return FORMAT.NONE;
	}

	private String directory, fileName;
	private String format;
	// param for HTML export
	private String htmlCss;
	private boolean htmlChapterTitle, htmlChapterBreakPage,
	   htmlNav, htmlNavImage, htmlMultiChapter, htmlMultiScene;
	private int highlight = Html.EM_LEFTASIS;
	// param for CSV export
	private String csvQuote = "\"", csvComma = ";";
	// param for TXT export
	private boolean txtTab;
	private String txtSeparator;

	@SuppressWarnings("OverridableMethodCallInConstructor")
	public BookParamExport(BookParam param) {
		super(param, "export");
		init();
	}

	/**
	 * initialize the parameters
	 */
	@Override
	protected void init() {
		Book p = param.book;
		setDirectory(p.getString(Book.PARAM.EXPORT_DIR));
		setHtmlChapterTitle(p.getBoolean(Book.PARAM.EXPORT_CHAPTER_BOOKTITLE));
		setHtmlChapterBreakPage(p.getBoolean(Book.PARAM.EXPORT_CHAPTER_BREAKPAGE));
		setHtmlCss(p.getString(Book.PARAM.EXPORT_HTML_CSS));
		setHtmlNav(p.getBoolean(Book.PARAM.EXPORT_HTML_NAV));
		setHtmlNavImage(p.getBoolean(Book.PARAM.EXPORT_HTML_NAVIMAGE));
		setHtmlMultiChapter(p.getBoolean(Book.PARAM.EXPORT_HTML_MULTICHAPTER));
		setHtmlMultiScene(p.getBoolean(Book.PARAM.EXPORT_HTML_MULTISCENE));
		setFormat(p.getString(Book.PARAM.EXPORT_FORMAT));
		setCsvQuote(p.getString(Book.PARAM.EXPORT_CSV_QUOTE));
		setCsvComma(p.getString(Book.PARAM.EXPORT_CSV_COMMA));
		setTxtTab(p.getBoolean(Book.PARAM.EXPORT_TXT_TAB));
		setTxtSeparator(p.getString(Book.PARAM.EXPORT_TXT_SEPARATOR));
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String d) {
		directory = d;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String d) {
		format = d;
	}

	public boolean getHtmlChapterTitle() {
		return htmlChapterTitle;
	}

	public void setHtmlChapterTitle(boolean d) {
		htmlChapterTitle = d;
	}

	public boolean getHtmlChapterBreakPage() {
		return htmlChapterBreakPage;
	}

	public void setHtmlChapterBreakPage(boolean d) {
		htmlChapterBreakPage = d;
	}

	public String getHtmlCss() {
		return htmlCss;
	}

	public void setHtmlCss(String d) {
		htmlCss = d;
	}

	public boolean isMulti() {
		return htmlMultiChapter || htmlMultiScene;
	}

	public boolean getHtmlMultiChapter() {
		return htmlMultiChapter;
	}

	public void setHtmlMultiChapter(boolean d) {
		htmlMultiChapter = d;
	}

	public boolean getHtmlMultiScene() {
		return htmlMultiScene;
	}

	public void setHtmlMultiScene(boolean d) {
		htmlMultiScene = d;
	}

	public boolean getHtmlNav() {
		return htmlNav;
	}

	public void setHtmlNav(boolean d) {
		htmlNav = d;
	}

	public boolean getHtmlNavImage() {
		return htmlNavImage;
	}

	public void setHtmlNavImage(boolean d) {
		htmlNavImage = d;
	}

	public void setHighlight(int value) {
		this.highlight = value;
	}

	public int getHighlight() {
		return this.highlight;
	}

	@Override
	public void refresh() {
		init();
	}

	@Override
	public void save() {
		//LOG.trace(TT + ".save()");
		Book p = param.book;
		if (!htmlNav) {
			htmlNavImage = false;
		}
		p.setString(Book.PARAM.EXPORT_DIR, directory);
		p.setBoolean(Book.PARAM.EXPORT_CHAPTER_BOOKTITLE, htmlChapterTitle);
		p.setBoolean(Book.PARAM.EXPORT_CHAPTER_BREAKPAGE, htmlChapterBreakPage);
		p.setBoolean(Book.PARAM.EXPORT_HTML_NAV, htmlNav);
		p.setBoolean(Book.PARAM.EXPORT_HTML_NAVIMAGE, htmlNavImage);
		p.setBoolean(Book.PARAM.EXPORT_HTML_MULTICHAPTER, htmlMultiChapter);
		p.setBoolean(Book.PARAM.EXPORT_HTML_MULTISCENE, htmlMultiScene);
		p.setString(Book.PARAM.EXPORT_HTML_CSS, htmlCss);
		p.setString(Book.PARAM.EXPORT_FORMAT, format);
		p.setString(Book.PARAM.EXPORT_CSV_QUOTE, csvQuote);
		p.setString(Book.PARAM.EXPORT_CSV_COMMA, csvComma);
		p.setBoolean(Book.PARAM.EXPORT_TXT_TAB, txtTab);
		p.setString(Book.PARAM.EXPORT_TXT_SEPARATOR, txtSeparator);
	}

	@Override
	public int hash() {
		return toXml().hashCode();
	}

	@Override
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(stringAttribute(0, KW.DIRECTORY.toString(), getDirectory()));
		b.append(stringAttribute(0, KW.FORMAT.toString(), getFormat()));
		b.append(stringAttribute(13, KW.HTML.toString(), getHtmlChapterTitle(), getHtmlChapterBreakPage(),
		   getHtmlMultiChapter(), getHtmlMultiScene(), getHtmlNav(), getHtmlNavImage())
		);
		b.append(stringAttribute(0, KW.CSS.toString(), getHtmlCss()));
		b.append(stringAttribute(13, KW.HIGHLIGHT.toString(), getHighlight()));
		String s = getCsvComma() + "," + getCsvQuote();
		b.append(stringAttribute(13, KW.CSV.toString(), (s.equals(",") ? "" : s)));
		b.append(stringAttribute(13, KW.TXT.toString(), getTxtSeparator() + "," + getTxtTab()));
		return b.toString();
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public boolean isHtml() {
		return format.equals("html");
	}

	public boolean isCsv() {
		return format.equals("csv");
	}

	public boolean isTxt() {
		return format.equals("txt");
	}

	public boolean isXml() {
		return format.equals("xml");
	}

	public boolean isOsbk() {
		return format.equals("osbk");
	}

	public String getTxtSeparator() {
		return this.txtSeparator;
	}

	public void setTxtSeparator(String value) {
		this.txtSeparator = value;
	}

	public boolean getTxtTab() {
		return txtTab;
	}

	public void setTxtTab(boolean sel) {
		txtTab = sel;
	}

	public void setCsvQuote(String value) {
		this.csvQuote = value;
	}

	public String getCsvQuote() {
		return this.csvQuote;
	}

	public void setCsvComma(String value) {
		this.csvComma = value;
	}

	public String getCsvComma() {
		return this.csvComma;
	}

	public BookParamLayout getLayout() {
		return this.param.getParamLayout();
	}

	public String toCheck() {
		StringBuilder b = new StringBuilder();
		b.append("format=").append(format).append("|");
		b.append("csvComma=").append(csvComma).append("|");
		b.append("csvQuote=").append(csvQuote).append("|");
		b.append("dir=").append(directory).append("|");
		b.append("fileName=").append(fileName).append("|");
		b.append("txtSeparator=").append(txtSeparator).append("|");
		b.append("txtTab=").append(txtTab ? "1" : "0");
		return b.toString();
	}

}
