/*
Copyright 2018, FaVdB
Modifications :

This file is part of oStorybook.

    oStorybook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    oStorybook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with oStorybook.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.book;

import i18n.I18N;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import storybook.model.hbn.entity.SbDate;
import storybook.model.hbn.entity.Status;

/**
 *
 * @author FaVdB
 */
public class BookInfo {

	private static final String TT = "BookInfo";

	private Book book;
	public String creation = "",
		maj = "",
		title = "",
		subtitle = "",
		author = "",
		copyright = "",
		UUID = "",
		ISBN = "",
		language = "",
		blurb = "",
		notes = "",
		dedication = "",
		assistant = "",
		assistantValue = "";
	private boolean scenario = false,
		markdown = false,
		calendarUse = false,
		review = false;
	private Status status;
	private Integer nature = 0;

	public BookInfo(Book book) {
		this.book = book;
		init();
	}

	public BookInfo(Book book, String name) {
		this(book);
		setTitle(name);
	}

	public void init() {
		setCreation(book.getString(Book.INFO.CREATION));
		setMaj(book.getString(Book.INFO.UPDATE));
		setTitle(book.getString(Book.INFO.TITLE));
		setSubtitle(book.getString(Book.INFO.SUBTITLE));
		setAuthor(book.getString(Book.INFO.AUTHOR));
		setCopyright(book.getString(Book.INFO.COPYRIGHT));
		setBlurb(book.getString(Book.INFO.BLURB));
		setNotes(book.getString(Book.INFO.NOTES));
		setDedication(book.getString(Book.INFO.DEDICATION));
		assistantSet(book.getString(Book.INFO.ASSISTANT));
		scenarioSet(book.getBoolean(Book.INFO.SCENARIO));
		setMarkdown(book.getBoolean(Book.INFO.MARKDOWN));
		setISBN(book.getString(Book.INFO.ISBN));
		setUUID(book.getString(Book.INFO.UUID));
		setLanguage(book.getString(Book.INFO.LANG));
		setNature(book.getInteger(Book.INFO.NATURE));
		setReview(book.getBoolean(Book.INFO.REVIEW));
	}

	public void save() {
		//LOG.trace(TT + ".save()");
		book.setString(Book.INFO.CREATION, getCreation());
		book.setString(Book.INFO.UPDATE, getMaj());
		book.setString(Book.INFO.TITLE, getTitle());
		book.setString(Book.INFO.SUBTITLE, getSubtitle());
		book.setString(Book.INFO.AUTHOR, getAuthor());
		book.setString(Book.INFO.COPYRIGHT, getCopyright());
		book.setString(Book.INFO.BLURB, getBlurb());
		book.setString(Book.INFO.NOTES, getNotes());
		book.setString(Book.INFO.DEDICATION, getDedication());
		book.setString(Book.INFO.ASSISTANT, assistantGet());
		book.setBoolean(Book.INFO.REVIEW, getReview());
		book.setBoolean(Book.INFO.SCENARIO, scenarioGet());
		book.setBoolean(Book.INFO.MARKDOWN, getMarkdown());
		book.setString(Book.INFO.ISBN, getISBN());
		book.setString(Book.INFO.UUID, getUUID());
		book.setString(Book.INFO.LANG, getLanguage());
		book.setInteger(Book.INFO.NATURE, getNature());
		book.setBoolean(Book.INFO.REVIEW, getReview());
		book.mainFrame.getBookModel().fires.fireAgainParts();
	}

	public void setCreation() {
		creation = SbDate.getToDay().getDateTimeToString();
		book.setString(Book.INFO.CREATION, creation);
	}

	public void setCreation(String c) {
		if (c.isEmpty()) {
			setCreation();
		} else {
			creation = c;
		}
		book.setString(Book.INFO.CREATION, c);
	}

	public String getCreation() {
		return (creation);
	}

	public Date getCreationDate() {
		return oneDate(creation);
	}

	public void setMaj() {
		maj = SbDate.getToDay().getDateTimeToString();
		book.setString(Book.INFO.UPDATE, maj);
	}

	public void setMaj(String c) {
		if (c.isEmpty()) {
			setMaj();
		}
		maj = c;
		book.setString(Book.INFO.UPDATE, maj);
	}

	public String getMaj() {
		return (maj);
	}

	public Date getMajDate() {
		return oneDate(maj);
	}

	public Date oneDate(String str) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
		String dateStr = str;
		if (str == null || str.isEmpty()) {
			dateStr = format.format(new Date());
		}
		Date date = new Date();
		try {
			date = format.parse(dateStr);
		} catch (ParseException e) {
		}
		return date;
	}

	public void setTitle(String s) {
		title = s;
		book.setString(Book.INFO.TITLE, s);
	}

	public String getTitle() {
		return (title);
	}

	public void setSubtitle(String s) {
		subtitle = s;
		book.setString(Book.INFO.SUBTITLE, s);
	}

	public String getSubtitle() {
		return (subtitle == null ? "" : subtitle);
	}

	public void setAuthor(String s) {
		author = s;
		book.setString(Book.INFO.AUTHOR, s);
	}

	public String getAuthor() {
		return (author == null ? "" : author);
	}

	public void setCopyright(String x) {
		copyright = x;
		book.setString(Book.INFO.COPYRIGHT, x);
	}

	public String getCopyright() {
		return (copyright == null ? "" : copyright);
	}

	public void setBlurb(String s) {
		blurb = s;
		book.setString(Book.INFO.BLURB, s);
	}

	public String getBlurb() {
		return blurb;
	}

	public void scenarioSet(boolean val) {
		scenario = val;
		book.setBoolean(Book.INFO.SCENARIO, val);
	}

	public boolean scenarioGet() {
		return scenario;
	}

	public void setISBN(String s) {
		ISBN = s;
		book.setString(Book.INFO.ISBN, s);
	}

	public String getISBN() {
		return ISBN;
	}

	public void setUUID(String s) {
		UUID = s;
		book.setString(Book.INFO.UUID, s);
	}

	public String getUUID() {
		return UUID;
	}

	public void setLanguage(String s) {
		language = s;
		book.setString(Book.INFO.LANG, s);
	}

	public String getLanguage() {
		String x = language;
		if (x.isEmpty()) {
			x = I18N.getCountryLanguage(Locale.getDefault()).substring(0, 2);
		}
		return (x);
	}

	public void setMarkdown(boolean x) {
		markdown = x;
		book.setBoolean(Book.INFO.MARKDOWN, x);
	}

	public boolean getMarkdown() {
		return (markdown);
	}

	public boolean isMarkdown() {
		return (markdown);
	}

	public void setReview(boolean value) {
		review = value;
	}

	public boolean getReview() {
		return review;
	}

	public void setNotes(String s) {
		notes = s;
		book.setString(Book.INFO.NOTES, s);
	}

	public String getNotes() {
		return notes;
	}

	public void setDedication(String s) {
		dedication = s;
		book.setString(Book.INFO.DEDICATION, s);
	}

	public String getDedication() {
		return dedication;
	}

	public void assistantSet(String s) {
		assistant = s;
		book.setString(Book.INFO.ASSISTANT, s);
	}

	public String assistantGet() {
		return assistant;
	}

	/**
	 * get nature of project:<br>
	 * <ul>
	 * <li>0=Other or undefined:</li>
	 * <li>1=Long Novel (more than 90000 words):90000</li>
	 * <li>2=Novel (more than 40000 words):40000</li>
	 * <li>3=Short Novel (more than 17500 words):17500</li>
	 * <li>4=Novela (more than 7500 words):7500</li>
	 * <li>5=Short Novela (less than 7500 words):</li>
	 * </ul>
	 *
	 * @return
	 */
	public int getNature() {
		return this.nature;
	}

	/**
	 * set the project nature
	 *
	 * @param nature
	 */
	public void setNature(int nature) {
		this.nature = Book.checkInteger(nature, 0, 5);
		book.setInteger(Book.INFO.NATURE, this.nature);
	}

	public static String isDataOK(String titre, String soustitre, String auteur, String droits) {
		String rc = "";
		String tolong = " " + I18N.getMsg("err.value.too.long");
		if (titre.isEmpty()) {
			rc += I18N.getMsg("book.title") + " " + I18N.getMsg("error.missing") + "\n";
		} else if (titre.length() > 128) {
			rc += I18N.getColonMsg("book.title") + tolong + "\n";
		}
		if (soustitre.length() > 256) {
			rc += I18N.getColonMsg("book.subtitle") + tolong + "\n";
		}
		if (auteur.length() > 256) {
			rc += I18N.getColonMsg("book.author") + tolong + "\n";
		}
		if (droits.length() > 256) {
			rc += I18N.getColonMsg("book.copyright") + tolong + "\n";
		}
		return (rc);
	}

}
