/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.handler;

import javax.swing.ListCellRenderer;
import storybook.model.book.Book;
import storybook.model.hbn.dao._GenericDAO;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.tools.LOG;
import storybook.ui.MainFrame;

/**
 * @author martin
 *
 */
public abstract class AbstractEntityHandler {

	protected MainFrame mainFrame;

	public AbstractEntityHandler(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

	public abstract <T> Class<T> getEntityClass();

	public abstract <T> Class<T> getDAOClass();

	public _GenericDAO<?, ?> createDAO() {
		try {
			return (_GenericDAO<?, ?>) getDAOClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			LOG.err("AbstractEntityHandler.createDAO() Exception:" + e.getMessage());
		}
		return null;
	}

	public boolean hasListCellRenderer() {
		return getListCellRenderer() != null;
	}

	public ListCellRenderer getListCellRenderer() {
		return null;
	}

	public static AbstractEntityHandler getHandler(MainFrame m, AbstractEntity entity) {
		AbstractEntityHandler handler = null;
		switch (Book.getTYPE(entity)) {
			case ATTRIBUTE:
				handler = new AttributeHandler(m);
				break;
			case CATEGORY:
				handler = new CategoryHandler(m);
				break;
			case CHAPTER:
				handler = new ChapterHandler(m);
				break;
			case EVENT:
				handler = new EventHandler(m);
				break;
			case GENDER:
				handler = new GenderHandler(m);
				break;
			case IDEA:
				handler = new IdeaHandler(m);
				break;
			case INTERNAL:
				handler = new InternalHandler(m);
				break;
			case ITEM:
				handler = new ItemHandler(m);
				break;
			case ITEMLINK:
				handler = new ItemlinkHandler(m);
				break;
			case LOCATION:
				handler = new LocationHandler(m);
				break;
			case MEMO:
				handler = new MemoHandler(m);
				break;
			case PART:
				handler = new PartHandler(m);
				break;
			case PLOT:
				handler = new PlotHandler(m);
				break;
			case PERSON:
				handler = new PersonHandler(m);
				break;
			case RELATION:
				handler = new RelationHandler(m);
				break;
			case SCENARIO:
			//handler = new ScenarioHandler(m);
			//break;
			case SCENE:
				handler = new SceneHandler(m);
				break;
			case STRAND:
				handler = new StrandHandler(m);
				break;
			case TAG:
				handler = new TagHandler(m);
				break;
			case TAGLINK:
				handler = new TaglinkHandler(m);
				break;
			default:
				break;
		}
		return (handler);
	}
}
