/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package storybook.model.handler;

import javax.swing.ListCellRenderer;
import storybook.model.hbn.dao.AttributeDAO;
import storybook.model.hbn.entity.Attribute;
import storybook.ui.MainFrame;
import storybook.ui.renderer.lcr.LCRAttribute;

/**
 *
 * @author favdb
 */
public class AttributeHandler extends AbstractEntityHandler {

	public AttributeHandler(MainFrame mainFrame) {
		super(mainFrame);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> Class<T> getEntityClass() {
		return (Class<T>) Attribute.class;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> Class<T> getDAOClass() {
		return (Class<T>) AttributeDAO.class;
	}
	
	@Override
	public ListCellRenderer getListCellRenderer() {
		return new LCRAttribute();
	}

}
