/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import java.sql.ResultSet;
import java.util.List;
import storybook.model.book.Book;

/**
 * @hibernate.subclass discriminator-value="20"
 */
public class Memo extends AbstractTag {

	public Memo() {
		super(Book.TYPE.MEMO, "010");
		type = TYPE_MEMO;
	}

	public Memo(ResultSet rs) {
		super(Book.TYPE.MEMO, "010", rs);
		type = TYPE_MEMO;
	}

	public static Memo find(List<Memo> list, String str) {
		for (Memo elem : list) {
			if (elem.getName().equals(str)) {
				return (elem);
			}
		}
		return (null);
	}

	public static Memo find(List<Memo> list, Long id) {
		for (Memo elem : list) {
			if (elem.id.equals(id)) {
				return (elem);
			}
		}
		return (null);
	}

	@Override
	public String toString() {
		return getName();
	}
	/*
	@Override
	@SuppressWarnings("unchecked")
	public String toDetail(boolean detailed) {
		StringBuilder b = new StringBuilder();
		b.append(toDetailHeader(detailed));
		b.append(toDetailFooter(detailed));
		return (b.toString());
	}

	@Override
	@SuppressWarnings("unchecked")
	public String toCsv(String quoteStart, String quoteEnd, String separator) {
		StringBuilder b = new StringBuilder();
		b.append(quoteStart).append(getId().toString()).append(quoteEnd).append(separator);
		b.append(quoteStart).append(getName()).append(quoteEnd).append(separator);
		b.append(quoteStart).append(getClean(getNotes())).append(quoteEnd).append("\n");
		b.append("\n");
		return (b.toString());
	}

	@Override
	@SuppressWarnings("unchecked")
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(toXmlTab(1)).append("<").append(getObjType().toString()).append(" \n");
		b.append(xmlCommon());
		b.append(xmlAttribute(DAOutil.NAME, getName()));
		b.append(toXmlTab(2)).append(">\n");
		b.append(toXmlEnd());
		return (b.toString());
	}*/

}
