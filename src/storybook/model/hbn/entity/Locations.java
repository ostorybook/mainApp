/*
 * Copyright (C) 2023 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import java.util.ArrayList;
import java.util.List;
import storybook.tools.ListUtil;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class Locations {

	private Locations() {
		//empty
	}

	/**
	 * find Locations linked to the given Chapter
	 *
	 * @param mainFrame
	 * @param chapter
	 * @return
	 */
	public static List find(MainFrame mainFrame, Chapter chapter) {
		List<Location> locations = new ArrayList<>();
		List<Scene> scenes = Scenes.findBy(mainFrame, chapter);
		for (Scene s : scenes) {
			for (Location l : s.getLocations()) {
				locations.add(l);
			}
		}
		return ListUtil.setUnique(locations);
	}

}
