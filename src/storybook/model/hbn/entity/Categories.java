/*
 * Copyright (C) 2023 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import storybook.model.Model;
import storybook.model.book.Book;
import static storybook.model.book.Book.TYPE.EVENT;
import static storybook.model.book.Book.TYPE.IDEA;
import static storybook.model.book.Book.TYPE.ITEM;
import static storybook.model.book.Book.TYPE.PLOT;
import static storybook.model.book.Book.TYPE.TAG;
import storybook.model.hbn.dao.EventDAO;
import storybook.model.hbn.dao.IdeaDAO;
import storybook.model.hbn.dao.ItemDAO;
import storybook.model.hbn.dao.PlotDAO;
import storybook.model.hbn.dao.TagDAO;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class Categories {

	private Categories() {

	}

	/**
	 * find all categories as a list of String for the given type
	 *
	 * @param mainFrame
	 * @param type
	 * @return
	 */
	public static List<String> find(MainFrame mainFrame, Book.TYPE type) {
		List<String> list = new ArrayList<>();
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		switch (type) {
			case EVENT:
				EventDAO eventDAO = new EventDAO(session);
				return (eventDAO.findCategories());
			case IDEA:
				IdeaDAO ideaDAO = new IdeaDAO(session);
				return (ideaDAO.findCategories());
			case ITEM:
				ItemDAO itemDAO = new ItemDAO(session);
				return (itemDAO.findCategories());
			case PLOT:
				PlotDAO plotDAO = new PlotDAO(session);
				return (plotDAO.findCategories());
			case TAG:
				TagDAO tagDAO = new TagDAO(session);
				return (tagDAO.findCategories());
			default:
				break;
		}
		return list;
	}

}
