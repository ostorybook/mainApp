/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import java.sql.ResultSet;
import java.util.List;
import org.hibernate.Session;
import org.w3c.dom.Node;
import storybook.model.book.Book;
import storybook.model.hbn.dao.ItemDAO;
import storybook.tools.xml.XmlKey.XK;
import storybook.tools.xml.XmlUtil;
import storybook.ui.MainFrame;

public class Item extends AbstractTag {
	//TODO intégrer ce qui est dans le AbstractTag pour rendre l'item indépendant

	public Item() {
		super(Book.TYPE.ITEM, "111");
		type = TYPE_ITEM;
	}

	public Item(ResultSet rs) {
		super(Book.TYPE.ITEM, "111", rs);
		type = TYPE_ITEM;
	}

	public static Item fromXml(Node node) {
		Item p = new Item();
		fromXmlBeg(node, p);
		p.setCategory(XmlUtil.getString(node, XK.CATEGORY));
		fromXmlEnd(node, p);
		return (p);
	}

	public static Item find(List<Item> list, String str) {
		for (Item elem : list) {
			if (elem.getName().equals(str)) {
				return (elem);
			}
		}
		return (null);
	}

	public static Item find(List<Item> list, Long id) {
		for (Item elem : list) {
			if (elem.id.equals(id)) {
				return (elem);
			}
		}
		return (null);
	}

	public static List<String> findCategories(MainFrame mainFrame) {
		@SuppressWarnings("unchecked")
		//List<Item> items = EntityUtil.findEntities(mainFrame, Book.TYPE.ITEM);
		Session session = mainFrame.getBookModel().beginTransaction();
		ItemDAO dao = new ItemDAO(session);
		List<String> list = dao.findCategories();
		session.close();
		return (list);
	}

	public static List<String> getDefColumns() {
		List<String> list = AbstractEntity.getDefColumns(Book.TYPE.ITEM);
		list.add("category, 256");
		return (list);
	}

}
