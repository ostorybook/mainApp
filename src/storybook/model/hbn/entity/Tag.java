/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import storybook.model.EntityUtil;
import storybook.model.book.Book;
import static storybook.model.hbn.entity.AbstractEntity.*;
import storybook.tools.ListUtil;
import storybook.tools.xml.XmlKey;
import storybook.tools.xml.XmlUtil;
import storybook.ui.MainFrame;

/**
 * @hibernate.subclass discriminator-value="0"
 */
public class Tag extends AbstractTag {

	public Tag() {
		super(Book.TYPE.TAG, "110");
		type = TYPE_TAG;
	}

	public Tag(ResultSet rs) {
		super(Book.TYPE.TAG, "111", rs);
		type = TYPE_TAG;
	}

	public static Tag fromXml(Node node) {
		Tag p = new Tag();
		fromXmlBeg(node, p);
		p.setCategory(XmlUtil.getString(node, XmlKey.XK.CATEGORY));
		fromXmlEnd(node, p);
		return p;
	}

	public static Tag find(List<Tag> list, String str) {
		for (Tag elem : list) {
			if (elem.getName().equals(str)) {
				return (elem);
			}
		}
		return (null);
	}

	public static Tag find(List<Tag> list, Long id) {
		for (Tag elem : list) {
			if (elem.id.equals(id)) {
				return (elem);
			}
		}
		return (null);
	}

	public static List<String> findCategories(MainFrame mainFrame) {
		@SuppressWarnings("unchecked")
		List<Tag> events = EntityUtil.findEntities(mainFrame, Book.TYPE.TAG);
		List<String> list = new ArrayList<>();
		events.forEach((e) -> {
			list.add(e.getCategory());
		});
		ListUtil.setUnique(list);
		return (list);
	}

}
