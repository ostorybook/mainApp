/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2023 FaVdB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import storybook.model.book.Book;
import storybook.tools.DateUtil;
import storybook.ui.panel.challenge.ChallengeData;

/**
 * class for the Challenges
 *
 * subclass discriminator-value="30"
 */
public class Challenge extends AbstractTag {

	private Date date = new Date();
	private Integer total = 0, duration = 0;
	private List<ChallengeData> datas = new ArrayList<>();

	/**
	 * create n empty Challenge
	 */
	public Challenge() {
		super(Book.TYPE.CHALLENGE, "010");
		type = TYPE_CHALLENGE;
		this.setName("challenge");
	}

	/**
	 * create a Challenge from a ResultSet
	 *
	 * @param rs
	 */
	@SuppressWarnings("OverridableMethodCallInConstructor")
	public Challenge(ResultSet rs) {
		this();
		loadData();
	}

	@Override
	public String toString() {
		return getName();
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Integer getTotal() {
		return this.total;
	}

	/**
	 * load the challenge datas
	 */
	public void loadData() {
		String lines[] = getNotes().split("\\n");
		// load date, duration, total
		String line[] = lines[0].split(";");
		this.date = DateUtil.stringToDate(line[0]);
		this.duration = Integer.valueOf(line[1]);
		this.total = Integer.valueOf(line[2]);
		// load datas
		datas = new ArrayList<>();
		for (int i = 1; i < lines.length; i++) {
			line = lines[i].split(";");
			Date d = DateUtil.stringToDate(line[0]);
			int nb = Integer.parseInt(line[1]);
			datas.add(new ChallengeData(d, nb));
		}
	}

	/**
	 * add a challenge data
	 *
	 * @param date
	 * @param nb
	 */
	public void addData(Date date, int nb) {
		for (ChallengeData cd : datas) {
			if (cd.getDate().equals(date)) {
				cd.setNb(nb);
				return;
			}
		}
		datas.add(new ChallengeData(date, nb));
	}

	public void saveData() {
		StringBuilder b = new StringBuilder();
		b.append(String.format("%s;%d,%d\n",
		   DateUtil.simpleDateToString(date), duration, total));
		for (ChallengeData cd : datas) {
			b.append(String.format("%s;%d\n",
			   DateUtil.simpleDateToString(cd.getDate()), cd.getNb()));
		}
	}

}
