/*
 * Copyright (C) 2023 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import storybook.model.Model;
import storybook.model.book.Book;
import static storybook.model.book.Book.TYPE.ITEM;
import static storybook.model.book.Book.TYPE.LOCATION;
import static storybook.model.book.Book.TYPE.PERSON;
import static storybook.model.book.Book.TYPE.SCENE;
import storybook.model.hbn.dao.RelationDAO;
import storybook.ui.MainFrame;

/**
 * utilities for Relationship
 *
 * @author favdb
 */
public class Relations {

	private Relations() {
		// empty
	}

	/**
	 * find Relationship linked with the given Entity
	 *
	 * @param mainFrame
	 * @param entity
	 * @return
	 */
	public static List<Relationship> find(MainFrame mainFrame, AbstractEntity entity) {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		RelationDAO dao = new RelationDAO(session);
		List<Relationship> links = new ArrayList<>();
		switch (Book.getTYPE(entity)) {
			case ITEM:
				links = dao.findByItems((Item) entity);
				break;
			case LOCATION:
				links = dao.findByLocations((Location) entity);
				break;
			case PERSON:
				links = dao.findByPersons((Person) entity);
				break;
			case SCENE:
				links = dao.findByStartOrEndScene((Scene) entity);
				break;
			default:
				break;
		}
		model.commit();
		return links;
	}

}
