/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import org.w3c.dom.Node;
import resources.icons.IconUtil;
import storybook.model.DB.DATA;
import storybook.model.book.Book;
import storybook.tools.swing.ColorIcon;
import storybook.tools.swing.ColorUtil;
import storybook.tools.xml.XmlKey.XK;
import storybook.tools.xml.XmlUtil;

/**
 * Strand generated by hbm2java
 *
 * @hibernate.class table="STRAND"
 */
public class Strand extends AbstractEntity {

	private String abbreviation = "";
	private Integer color = 0;
	private Integer sort = 0;

	public Strand() {
		super(Book.TYPE.STRAND, "111");
	}

	public Strand(ResultSet rs) {
		super(Book.TYPE.STRAND, "111", rs);
		try {
			abbreviation = rs.getString("abbreviation");
			color = rs.getInt("color");
			sort = rs.getInt("sort");
		} catch (SQLException ex) {
			//empty
		}
	}

	public Strand(String abbr, String name) {
		this();
		this.name = name;
		this.abbreviation = abbr;
		this.color = ColorUtil.PALETTE.LIGHT_BLUE.getColor().getRGB();
		this.sort = 1;
	}

	public Strand(Integer sort, String name, String abbr, Integer color, String notes) {
		this(abbr, name);
		this.color = color;
		this.sort = sort;
		setNotes(notes);
	}

	public String getAbbreviation() {
		return this.abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Integer getColor() {
		return (color == null ? 0 : color);
	}

	public Color getJColor() {
		if (color == null) {
			return null;
		}
		return new Color(color);
	}

	public static Color getJColor(Strand s) {
		if (s == null) {
			return null;
		}
		if (s.getColor() == null) {
			return null;
		}
		return new Color(s.getColor());
	}

	public String getHTMLColor() {
		return "<span style=\""
		   + "background-color:" + ColorUtil.getHTML(getJColor()) + ";"
		   + "color:" + ColorUtil.getHTML(getJColor()) + ";\">"
		   + "&#x25ae;&#x25ae;"
		   + "</span> " //+ ColorUtil.getHTML(getJColor())
		   ;
	}

	public ColorIcon getColorIcon() {
		return getColorIcon(IconUtil.getDefSize());
	}

	public ColorIcon getColorIcon(int sz) {
		return new ColorIcon(getJColor(), sz);
	}

	public void setColor(Integer color) {
		this.color = color;
	}

	public void setJColor(Color color) {
		if (color == null) {
			this.color = null;
			return;
		}
		this.color = color.getRGB();
	}

	public Integer getSort() {
		if (sort == null) {
			return (0);
		}
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public static Strand findSort(List<Strand> strands, Integer n) {
		for (Strand s : strands) {
			if (s.getSort().equals(n)) {
				return (s);
			}
		}
		return (null);
	}

	public static Integer getNextNumber(List<Strand> strands) {
		int n = 0;
		for (Strand s : strands) {
			if (s.getSort() > n) {
				n = s.getSort();
			}
		}
		return (n + 1);
	}

	@Override
	public Icon getIcon() {
		return getIcon(IconUtil.getDefSize());
	}

	@Override
	public String getAbbr() {
		return abbreviation;
	}

	@Override
	public String toString() {
		if (isTransient()) {
			return ("");
		}
		return getName() + " (" + getAbbr() + ")" + (hasNotes() ? "*" : "");
	}

	@Override
	public String toCsv(String quoteStart, String quoteEnd, String separator) {
		StringBuilder b = new StringBuilder();
		b.append(quoteStart).append(getClean(this)).append(quoteEnd).append(separator);
		b.append(quoteStart).append(getClean(abbreviation)).append(quoteEnd).append(separator);
		b.append(quoteStart).append(getClean(getName())).append(quoteEnd).append(separator);
		b.append(quoteStart).append(getClean(getColor())).append(quoteEnd).append(separator);
		b.append(quoteStart).append(getClean(getSort())).append(quoteEnd).append(separator);
		b.append(quoteStart).append(getClean(getNotes())).append(quoteStart).append("\n");
		return (b.toString());
	}

	@Override
	public String toHtml() {
		return (toCsv("<td>", "</td>", "\n"));
	}

	@Override
	public String toText() {
		return (toCsv("", "", "\t"));
	}

	@Override
	public String toDetail(Integer detailed) {
		StringBuilder b = new StringBuilder();
		b.append(toDetailHeader(detailed));
		b.append(getInfo(detailed, DATA.ABBREVIATION, getAbbreviation()));
		b.append(getInfo(detailed, DATA.STRAND_COLOR, getHTMLColor()));
		b.append(getInfo(detailed, DATA.SORT, getClean(getSort())));
		b.append(toDetailFooter(detailed));
		return (b.toString());
	}

	@Override
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(toXmlBeg());
		b.append(XmlUtil.setAttribute(XK.ABBREVIATION, getAbbreviation()));
		b.append(XmlUtil.setAttribute(XK.COLOR, getClean(getColor())));
		b.append(XmlUtil.setAttribute(XK.SORT, getClean(getSort())));
		b.append(">\n");
		b.append(toXmlEnd());
		/*if (!getNotes().isEmpty()) {
			b.append(toXmlMeta(2, "notes", getNotes(), false));
		}*/
		return XmlUtil.format(b.toString());
	}

	public static Strand fromXml(Node node) {
		Strand p = new Strand();
		fromXmlBeg(node, p);
		p.setAbbreviation(XmlUtil.getString(node, XK.ABBREVIATION));
		p.setColor(XmlUtil.getInteger(node, XK.COLOR));
		p.setSort(XmlUtil.getInteger(node, XK.SORT));
		fromXmlEnd(node, p);
		return (p);
	}

	@Override
	@SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Strand test = (Strand) obj;
		boolean ret = true;
		ret = ret && equalsStringNullValue(getName(), test.getName());
		ret = ret && equalsStringNullValue(abbreviation, test.getAbbreviation());
		ret = ret && equalsIntegerNullValue(color, test.getColor());
		ret = ret && equalsIntegerNullValue(sort, test.getSort());
		ret = ret && equalsStringNullValue(getNotes(), test.getNotes());
		return ret;
	}

	@Override
	public int hashCode() {
		return hashPlus(super.hashCode(),
		   abbreviation,
		   color,
		   sort);
	}

	@Override
	public int compareTo(AbstractEntity en) {
		Strand o = (Strand) en;
		if (o == null) {
			return 0;
		}
		if (sort == null && o.getSort() != null) {
			return -1;
		}
		if (sort != null && o.getSort() == null) {
			return 1;
		}
		return sort.compareTo(o.getSort());
	}

	public static Strand find(List<Strand> list, String str) {
		for (Strand elem : list) {
			if (elem.getName().equals(str)) {
				return (elem);
			}
		}
		return (null);
	}

	public static Strand find(List<Strand> list, Long id) {
		for (Strand elem : list) {
			if (elem.id.equals(id)) {
				return (elem);
			}
		}
		return (null);
	}

	public static List<String> getDefColumns() {
		List<String> list = AbstractEntity.getDefColumns(Book.TYPE.STRAND);
		list.add("abbreviation, 256");
		return (list);
	}

	public static List<String> getTable() {
		List<String> ls = new ArrayList<>();
		String tableName = "strand";
		AbstractEntity.getTable(tableName, ls);
		ls.add(tableName + ",abbreviation,String,256");
		ls.add(tableName + ",color,Integer,0");
		ls.add(tableName + ",sort,Integer,0");
		return (ls);
	}

}
