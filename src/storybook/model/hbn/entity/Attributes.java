/*
 * Copyright (C) 2023 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import java.util.List;
import org.hibernate.Session;
import storybook.model.Model;
import storybook.model.hbn.dao.AttributeDAO;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class Attributes {

	private Attributes() {
		//empty
	}

	/**
	 * find the Attribute of the given key name and value
	 *
	 * @param mainFrame
	 * @param key
	 * @param value
	 * @return
	 */
	public static Attribute find(MainFrame mainFrame, String key, String value) {
		Model model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		AttributeDAO dao = new AttributeDAO(session);
		List<Attribute> attributes = dao.findAll();
		model.commit();
		for (Attribute attribute : attributes) {
			if (key.equals(attribute.getKey()) && value.equals(attribute.getValue())) {
				return (attribute);
			}
		}
		return (null);
	}

}
