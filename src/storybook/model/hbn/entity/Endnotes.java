/*
 * Copyright (C) 2023 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

/**
 *
 * @author favdb
 */
public class Endnotes {

	private Endnotes() {
		// empty
	}

	/**
	 * check if the given Ednote is in the text of the Scene
	 *
	 * @param endnote
	 * @return
	 */
	public static boolean check(Endnote endnote) {
		@SuppressWarnings("unchecked")
		Scene scene = endnote.getScene();
		if (scene == null) {
			return false;
		}
		return endnote.isInText();
	}

}
