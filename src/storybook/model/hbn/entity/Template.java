/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import storybook.model.book.Book;
import static storybook.model.hbn.entity.AbstractEntity.*;
import storybook.tools.xml.XmlUtil;

public class Template extends AbstractEntity {

	public Template() {
		super(Book.TYPE.NONE, "000");
	}

	public Template(String name, String notes) {
		this();
		setName(name);
		setNotes(notes);
	}

	@Override
	@SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Template test = (Template) obj;
		boolean ret = true;
		return ret;
	}

	@Override
	public int hashCode() {
		return hashPlus(super.hashCode());
	}

	@Override
	public String toString() {
		if (isTransient()) {
			return "";
		}
		return getName() + (hasNotes() ? "*" : "");
	}

	@Override
	public String toCsv(String quoteStart, String quoteEnd, String separator) {
		StringBuilder b = new StringBuilder();
		b.append(quoteStart).append(getId().toString()).append(quoteEnd).append(separator);
		b.append(quoteStart).append(getName()).append(quoteEnd).append(separator);
		b.append(quoteStart).append(getClean(getNotes())).append(quoteEnd).append(separator);
		b.append("\n");
		return (b.toString());
	}

	@Override
	public String toHtml() {
		return (toCsv("<td>", "</td>", "\n"));
	}

	@Override
	public String toText() {
		return (toCsv("", "", "\t"));
	}

	@Override
	public String toDetail(Integer detailed) {
		StringBuilder b = new StringBuilder();
		b.append(toDetailHeader(detailed));
		b.append(toDetailFooter(detailed));
		return (b.toString());
	}

	@Override
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append("<event \n");
		b.append(xmlCommon());
		b.append(">\n");
		b.append(toXmlMeta(2, "notes", getClean(getNotes()), false));
		b.append("</event>\n");
		return (b.toString());
	}

	public static Template fromXml(Node node) {
		Template p = new Template();
		p.setId(XmlUtil.getLong(node, "id"));
		p.setName(XmlUtil.getString(node, "name"));
		p.setNotes(XmlUtil.getText(node, "notes"));
		return (p);
	}

	public static List<String> getDefColumns() {
		List<String> list = AbstractEntity.getDefColumns(Book.TYPE.NONE);
		return (list);
	}

	public static List<String> getTable() {
		List<String> ls = new ArrayList<>();
		String tableName = "template";
		AbstractEntity.getTable(tableName, ls);
		return (ls);
	}

}
