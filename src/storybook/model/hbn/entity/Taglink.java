/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import i18n.I18N;
import java.sql.ResultSet;
import java.util.List;
import org.w3c.dom.Node;
import storybook.model.book.Book;
import storybook.model.DB.DATA;
import storybook.tools.xml.XmlUtil;

/**
 * @hibernate.subclass discriminator-value="0"
 */
public class Taglink extends AbstractTaglink {

	private Tag tag = new Tag();

	public Taglink() {
		super(Book.TYPE.TAGLINK, "010");
		type = TYPE_TAG;
	}

	public Taglink(ResultSet rs) {
		super(Book.TYPE.TAGLINK, "010", rs);
		type = TYPE_TAG;
	}

	@SuppressWarnings("OverridableMethodCallInConstructor")
	public Taglink(Tag tag, Integer type, Scene startScene, Scene endScene,
	   Person person, Location location) {
		this();
		setTag(tag);
		setType(type);
		setStartScene(startScene);
		setEndScene(endScene);
		setPerson(person);
		setLocation(location);
	}

	@Override
	public String getName() {
		if (super.getName().isEmpty()) {
			if (getPerson() != null && getLocation() != null) {
				setName(getPerson().getName().trim() + " / " + getLocation().getName().trim());
			} else if (getPerson() != null) {
				setName(getPerson().getName().trim());
			} else if (getLocation() != null) {
				setName(getLocation().getName().trim());
			}
			setName(I18N.getMsg(getObjType().toString()) + "=>" + super.getName());
		}
		return super.getName();
	}

	public Tag getTag() {
		return this.tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	@Override
	public String toDetail(Integer detailed) {
		StringBuilder b = new StringBuilder();
		b.append(toDetailHeader(detailed));
		b.append(getInfo(2, DATA.TAG, getTag()));
		b.append(super.toDetail(detailed));
		b.append(toDetailFooter(detailed));
		return (b.toString());
	}

	public static Taglink fromXml(Node node) {
		Taglink p = new Taglink();
		p.setId(XmlUtil.getLong(node, "id"));
		return (p);
	}

	@Override
	@SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Taglink test = (Taglink) obj;
		boolean ret = true;
		ret = ret && equalsLongNullValue(tag.id, test.getTag().getId());
		return ret;
	}

	@Override
	public int hashCode() {
		return hashPlus(super.hashCode(), tag);
	}

	public static Taglink find(List<Taglink> list, String str) {
		for (Taglink elem : list) {
			if (elem.getName().equals(str)) {
				return (elem);
			}
		}
		return (null);
	}

	public static Taglink find(List<Taglink> list, Long id) {
		for (Taglink elem : list) {
			if (elem.id.equals(id)) {
				return (elem);
			}
		}
		return (null);
	}

}
