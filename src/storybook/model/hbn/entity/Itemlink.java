/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.entity;

import i18n.I18N;
import java.sql.ResultSet;
import java.util.List;
import org.w3c.dom.Node;
import storybook.model.DB.DATA;
import storybook.model.book.Book;
import storybook.tools.xml.XmlUtil;

public class Itemlink extends AbstractTaglink {

	private Item item = new Item();

	public Itemlink() {
		super(Book.TYPE.ITEMLINK, "010");
		type = TYPE_ITEM;
	}

	public Itemlink(ResultSet rs) {
		super(Book.TYPE.ITEMLINK, "010", rs);
		type = TYPE_ITEM;
	}

	@SuppressWarnings("OverridableMethodCallInConstructor")
	public Itemlink(Item item, Integer type, Scene startScene, Scene endScene, Person person, Location location) {
		this();
		setItem(item);
		setType(type);
		setStartScene(startScene);
		setEndScene(endScene);
		setPerson(person);
		setLocation(location);
	}

	@Override
	public String getName() {
		String x = super.getName();
		if (x.isEmpty()) {
			if (getPerson() != null && getLocation() != null) {
				setName(getPerson().getName().trim() + " / " + getLocation().getName().trim());
			} else if (getPerson() != null) {
				setName(getPerson().getName().trim());
			} else if (getLocation() != null) {
				setName(getLocation().getName().trim());
			}
			setName(I18N.getMsg(getObjType().toString()) + "=>" + super.getName());
		}
		return super.getName();
	}

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	@Override
	public String toDetail(Integer detailed) {
		StringBuilder b = new StringBuilder();
		b.append(toDetailHeader(detailed));
		b.append(getInfo(2, DATA.ITEM, getItem()));
		b.append(super.toDetail(detailed));
		b.append(toDetailFooter(detailed));
		return (b.toString());
	}

	public static Itemlink fromXml(Node node) {
		Itemlink p = new Itemlink();
		p.setId(XmlUtil.getLong(node, "id"));
		return (p);
	}

	@Override
	@SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Itemlink test = (Itemlink) obj;
		boolean ret = true;
		ret = ret && equalsLongNullValue(item.id, test.getItem().getId());
		return ret;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (item != null ? item.hashCode() : 0);
		return hash;
	}

	public static Itemlink find(List<Itemlink> list, String str) {
		for (Itemlink elem : list) {
			if (elem.getName().equals(str)) {
				return (elem);
			}
		}
		return (null);
	}

	public static Itemlink find(List<Itemlink> list, Long id) {
		for (Itemlink elem : list) {
			if (elem.id.equals(id)) {
				return (elem);
			}
		}
		return (null);
	}

}
