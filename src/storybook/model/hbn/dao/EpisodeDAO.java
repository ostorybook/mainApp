/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import storybook.model.hbn.entity.Episode;
import storybook.model.hbn.entity.Strand;

public class EpisodeDAO extends _GenericDAO<Episode, Long>
		implements EpisodeDAOInterface {

	public static final String SELECT_EPISODE = "select s from Episode as s";

	public EpisodeDAO() {
		super();
	}

	public EpisodeDAO(Session session) {
		super(session);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Episode> findAll() {
		Query query = session
				.createQuery(SELECT_EPISODE
						+ " order by s.number, s.strand");
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Episode> findAll(Strand strand) {
		if (strand == null) {
			return new ArrayList<>();
		}
		Query query = session.createQuery(SELECT_EPISODE
				+ " where s.strand=:strand");
		query.setEntity(DAOutil.STRAND, strand);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Episode> findAll(Integer number) {
		if (number == null) {
			return new ArrayList<>();
		}
		Query query = session.createQuery(SELECT_EPISODE
				+ " where s.number=:number");
		query.setParameter("number", number);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Episode> findByStrand(Strand strand) {
		Query query = session.createQuery(SELECT_EPISODE
				+ " where s.strand=:strand");
		query.setParameter(DAOutil.STRAND, strand);
		return query.list();
	}

}

interface EpisodeDAOInterface extends GenericDAO<Episode, Long> {
}
