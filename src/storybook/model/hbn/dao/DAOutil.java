/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.dao;

import storybook.model.book.Book;
import storybook.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class DAOutil {

	public static final String KEY = "key",
			ID = "id",
			ASSISTANT = "assistant",
			ATTRIBUTE = "attribute",
			CATEGORY = "category",
			CHAPTER = "chapter",
			CHAPTERNO = "chapterno",
			DATE = "date",
			DESCRIPTION = "description",
			END = "end",
			EPISODE = "episode",
			FILE = "file",
			FILE_ICON = "icon",
			FIRSTNAME = "firstname",
			GENDER = "gender",
			ITEM = "item",
			ITEMS = "items",
			LASTNAME = "lastname",
			LOCATION = "location",
			LOCATIONS = "locations",
			NAME = "name",
			NARRATOR = "narrotor",
			NOTES = "notes",
			NUMBER = "number",
			PART = "part",
			PERSON = "person",
			PERSONS = "persons",
			PLOT = "plot",
			PLOTS = "plots",
			SCENARIO_LOC = "loc",
			SCENARIO_MOMENT = "moment",
			SCENARIO_PITCH = "pitch",
			SCENE_DURATION = "duration",
			SCENE_END = "endScene",
			SCENE_INTENSITY = "intensity",
			SCENE_NO = "sceneno",
			SCENE = "scene",
			SCENE_STAGE = "stage",
			SCENE_START = "startScene",
			SORT = "sort",
			START = "start",
			STATUS = "status",
			STRAND = "strand",
			TAG = "tag",
			UUID = "uuid";

	private DAOutil() {
		throw new IllegalStateException("Utility class");
	}

	public static _GenericDAO getDAO(MainFrame m, Book.TYPE type) {
		switch (type) {
			case ATTRIBUTE:
				return (new AttributeDAO(m.getSession()));
			case CATEGORY:
				return (new CategoryDAO(m.getSession()));
			case CHAPTER:
				return (new ChapterDAO(m.getSession()));
			case ENDNOTE:
				return (new EndnoteDAO(m.getSession()));
			case EPISODE:
				return (new EpisodeDAO(m.getSession()));
			case EVENT:
				return (new EventDAO(m.getSession()));
			case GENDER:
				return (new GenderDAO(m.getSession()));
			case IDEA:
				return (new IdeaDAO(m.getSession()));
			case INTERNAL:
				return (new InternalDAO(m.getSession()));
			case ITEM:
				return (new ItemDAO(m.getSession()));
			case ITEMLINK:
				return (new ItemlinkDAO(m.getSession()));
			case LOCATION:
				return (new LocationDAO(m.getSession()));
			case MEMO:
				return (new MemoDAO(m.getSession()));
			case PART:
				return (new PartDAO(m.getSession()));
			case PERSON:
				return (new PersonDAO(m.getSession()));
			case PLOT:
				return (new PlotDAO(m.getSession()));
			case RELATION:
				return (new RelationDAO(m.getSession()));
			case SCENE:
				return (new SceneDAO(m.getSession()));
			case STRAND:
				return (new StrandDAO(m.getSession()));
			case TAG:
				return (new TagDAO(m.getSession()));
			case TAGLINK:
			default:
				return (new TaglinkDAO(m.getSession()));
		}
	}

	public static AttributeDAO getAttributeDAO(MainFrame m) {
		return (new AttributeDAO(m.getSession()));
	}

	public static CategoryDAO getCategoryDAO(MainFrame m) {
		return (new CategoryDAO(m.getSession()));
	}

	public static ChapterDAO getChapterDAO(MainFrame m) {
		return (new ChapterDAO(m.getSession()));
	}

	public static EndnoteDAO getEndnoteDAO(MainFrame m) {
		return (new EndnoteDAO(m.getSession()));
	}

	public static EpisodeDAO getEpisodeDAO(MainFrame m) {
		return (new EpisodeDAO(m.getSession()));
	}

	public static EventDAO getEventDAO(MainFrame m) {
		return (new EventDAO(m.getSession()));
	}

	public static GenderDAO getGenderDAO(MainFrame m) {
		return (new GenderDAO(m.getSession()));
	}

	public static IdeaDAO getIdeaDAO(MainFrame m) {
		return (new IdeaDAO(m.getSession()));
	}

	public static InternalDAO getInternalDAO(MainFrame m) {
		return (new InternalDAO(m.getSession()));
	}

	public static ItemDAO getItemDAO(MainFrame m) {
		return (new ItemDAO(m.getSession()));
	}

	public static ItemlinkDAO getItemlinkDAO(MainFrame m) {
		return (new ItemlinkDAO(m.getSession()));
	}

	public static LocationDAO getLocationDAO(MainFrame m) {
		return (new LocationDAO(m.getSession()));
	}

	public static MemoDAO getMemoDAO(MainFrame m) {
		return (new MemoDAO(m.getSession()));
	}

	public static PartDAO getPartDAO(MainFrame m) {
		return (new PartDAO(m.getSession()));
	}

	public static PersonDAO getPersonDAO(MainFrame m) {
		return (new PersonDAO(m.getSession()));
	}

	public static PlotDAO getPlotDAO(MainFrame m) {
		return (new PlotDAO(m.getSession()));
	}

	public static RelationDAO getRelationDAO(MainFrame m) {
		return (new RelationDAO(m.getSession()));
	}

	public static SceneDAO getSceneDAO(MainFrame m) {
		return (new SceneDAO(m.getSession()));
	}

	public static StrandDAO getStrandDAO(MainFrame m) {
		return (new StrandDAO(m.getSession()));
	}

	public static TagDAO getTagDAO(MainFrame m) {
		return (new TagDAO(m.getSession()));
	}

	public static TaglinkDAO getTaglinkDAO(MainFrame m) {
		return (new TaglinkDAO(m.getSession()));
	}

}
