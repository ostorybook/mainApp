/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import storybook.model.hbn.entity.Location;
import storybook.model.hbn.entity.Person;
import storybook.model.hbn.entity.Scene;
import storybook.model.hbn.entity.Tag;
import storybook.model.hbn.entity.Taglink;

public class TaglinkDAO extends _GenericDAO<Taglink, Long>
		implements TaglinkDAOInterface {

	public TaglinkDAO() {
		super();
	}

	public TaglinkDAO(Session session) {
		super(session);
	}

	@SuppressWarnings("unchecked")
	public List<Taglink> findByTag(Tag tag) {
		Criteria crit = session.createCriteria(Taglink.class);
		crit.add(Restrictions.eq(DAOutil.TAG, tag));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Taglink> findByScene(Scene scene) {
		Criteria crit = session.createCriteria(Taglink.class);
		crit.add(Restrictions.eq(DAOutil.SCENE_START, scene));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Taglink> findByStartOrEndScene(Scene scene) {
		Criteria crit = session.createCriteria(Taglink.class);
		Criterion cr1 = Restrictions.eq(DAOutil.SCENE_START, scene);
		Criterion cr2 = Restrictions.eq(DAOutil.SCENE_END, scene);
		crit.add(Restrictions.or(cr1, cr2));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Taglink> findByPerson(Person person) {
		Criteria crit = session.createCriteria(Taglink.class);
		crit.add(Restrictions.eq(DAOutil.PERSON, person));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Taglink> findByLocation(Location location) {
		Criteria crit = session.createCriteria(Taglink.class);
		crit.add(Restrictions.eq(DAOutil.LOCATION, location));
		return crit.list();
	}

}

interface TaglinkDAOInterface extends GenericDAO<Taglink, Long> {
}
