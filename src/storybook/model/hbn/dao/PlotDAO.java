/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.hbn.entity.Plot;
import storybook.model.hbn.entity.Scene;

public class PlotDAO extends _GenericDAO<Plot, Long>
		implements PlotDAOInterface {

	public PlotDAO() {
		super();
	}

	public PlotDAO(Session session) {
		super(session);
	}

	public Plot findTitle(String str) {
		String nstr = str.trim();
		List<Plot> list = findAll();
		for (Plot p : list) {
			if (p.getName().trim().equals(nstr)) {
				return (p);
			}
		}
		return (null);
	}

	@SuppressWarnings("unchecked")
	public List<Plot> findAllByName() {
		Criteria crit = session.createCriteria(Plot.class);
		crit.addOrder(Order.asc(DAOutil.NAME));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Plot> findAllOrderBySort() {
		Criteria crit = session.createCriteria(Plot.class);
		crit.addOrder(Order.asc("sort"));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<String> findAllInList() {
		Query query = session.createQuery("from Action"
				+ " order by name");
		List<String> list = new ArrayList<>();
		for (Plot s : (List<Plot>) query.list()) {
			list.add(s.getName());
		}
		return (list);
	}

	@SuppressWarnings("unchecked")
	public List<Scene> findScenes(Plot action) {
		Query query = session
				.createQuery("select s from Scene as s"
						+ " left join s.chapter as ch"
						+ " where s.action=:action"
						+ " order by ch.chapterno, s.sceneno");
		query.setEntity("action", action);
		return (List<Scene>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<String> findCategories() {
		Query query = session
				.createQuery("select distinct(i.category)"
						+ " from Plot as i"
						+ " order by i.category");
		return (List<String>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Plot> findByCategory(String category) {
		Criteria crit = session.createCriteria(Plot.class);
		crit.add(Restrictions.eq(DAOutil.CATEGORY, category));
		crit.addOrder(Order.asc(DAOutil.NAME));
		return (List<Plot>) crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<AbstractEntity> findAllByCategory() {
		Criteria crit = session.createCriteria(Plot.class);
		crit.addOrder(Order.asc(DAOutil.CATEGORY));
		crit.addOrder(Order.asc(DAOutil.NAME));
		return (List<AbstractEntity>) crit.list();
	}

}

interface PlotDAOInterface extends GenericDAO<Plot, Long> {
}
