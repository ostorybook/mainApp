/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package storybook.model.hbn.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import storybook.model.hbn.entity.AbstractEntity;
import storybook.model.hbn.entity.Status;

public class StatusDAO extends _GenericDAO<Status, Long>
		implements StatusDAOInterface {

	public StatusDAO() {
		super();
	}

	public StatusDAO(Session session) {
		super(session);
	}

	public Status findTitle(String str) {
		String nstr = str.trim();
		List<Status> list = findAll();
		for (Status p : list) {
			if (p.getName().trim().equals(nstr)) {
				return (p);
			}
		}
		return (null);
	}

	@SuppressWarnings("unchecked")
	public List<AbstractEntity> findAllByName() {
		Criteria crit = session.createCriteria(Status.class);
		crit.addOrder(Order.asc(DAOutil.NAME));
		return crit.list();
	}

}

interface StatusDAOInterface extends GenericDAO<Status, Long> {
}
