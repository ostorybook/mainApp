#!/bin/sh
            echo DEBIAN package for 5.61
            rm -r -f /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package
            mkdir /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package
            echo Copy debian-package structure
            cp -R /home/favdb/oStorybook/mainApp/deployment/linux/debian/DEBIAN /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package
            cp -R /home/favdb/oStorybook/mainApp/deployment/linux/debian/usr /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package
            sed -i -e "s/@@version@@/5.61/g" /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package/DEBIAN/control
            sed -i -e "s/@@version@@/5.61/g" /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package/usr/share/applications/ostorybook.desktop
            sed -i -e "s/(version)/(5.61)/g" /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package/usr/share/doc/ostorybook/changelog
            gzip -9 /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package/usr/share/doc/ostorybook/changelog
            echo Copy application
            cp -R /home/favdb/oStorybook/mainApp/Assistant /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package/usr/share/ostorybook/
            cp /home/favdb/oStorybook/mainApp/distrib/5.61/single_oStorybook.jar /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package/usr/share/ostorybook/oStorybook.jar
            echo Modification for permissions
            find /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package/usr -type d -exec chmod 755 {} +
            chmod -R 755 /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package/DEBIAN
            echo set the md5
            echo build of Debian package
            dpkg-deb --build /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package /home/favdb/oStorybook/mainApp/distrib/5.61/oStorybook-5.61.deb
            echo Cleaning...
            rm -r -f /home/favdb/oStorybook/mainApp/distrib/5.61/debian-package
            echo Build RPM package
            cd /home/favdb/oStorybook/mainApp/distrib/5.61
            alien -r /home/favdb/oStorybook/mainApp/distrib/5.61/oStorybook-5.61.deb
            mv /home/favdb/oStorybook/mainApp/distrib/5.61/ostorybook-5.61*.rpm oStorybook-5.61.rpm
            lintian -i -I /home/favdb/oStorybook/mainApp/distrib/5.61/oStorybook-5.61.deb
        