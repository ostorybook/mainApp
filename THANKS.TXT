Storybook is developed with several "external" API and tools

NetBeans : version 11.3
http://netbeans.apache.org
Integrated Development Environment

Eclipse :
http://www.eclipse.org
Integrated Development Environment

Commons BeanUtils: Apache License 2.0
Version: 1.9.4
https://commons.apache.org/proper/commons-beanutils/
Apache Commons BeanUtils fournit une interface facile à utiliser mais 
flexible pour la réflexion et l'introspection.
Library: commons-beanutils

DOM4J : Copyright 2001-2005 (C) MetaStuff
Version: 1.6.1 (20/05/2005)
http://sourceforge.net/projects/dom4j
XML framework for Java
Library: dom4j

FlatLaf: FormDev Software, Apache License 2.0
Version: 2.1
https://www.formdev.com/flatlaf/
Look and Feel for Java Swing desktop applications
Library: flatlaf-2.1.jar
Source code: https://github.com/JFormDesigner/FlatLaf

GenericDAO : szczytowski, licence Apache 2.0
Version: 1.1.3 (23/01/2012)
http://code.google.com/p/generic-dao/
DAO manager
Library: search-1.0.0, search-hibernate-1.0.0, dao-1.0.0, dao-hibernate

H2 database :
Version: 1.4.200 beta (14/10/2019)
Multiple-Licensed under the H2 License, Version 1.0,
and under the Eclipse Public License, Version 1.0
(http://h2database.com/html/license.html).
Initial Developer: H2 Group
H2database database engine
Note: the source code is embedded, it is version 1.4.189, do not upgrade

Hibernate: Apache
Version: 3.1
Apache licence
http://hibernate.org/
Gestion de la persistance des objets en base de données relationnelle.

IDW-GPL : GPL v2
InfoNode Docking Windows is developed by NNL Technology AB.
Version: 1.6.1 (25/02/2009)
<http://www.infonode.net>
Advanced GUIs for Java applications.

JCalendar: LGPL, by Kai Tödter
Version: 2004
https://toedter.com/jcalendar/
Date chooser bean for graphically picking a date.

JOrtho : GPL
Version: 1.0 (11/03/2013)
(https://www.inetsoftware.de/other-products/jortho)
Open Source spell-checker
Note: the source code is embedded, do not upgrade

Jsoup : Jonathan Hedley, liberal MIT license
Version: 1.13.1 (29/02/2020)
http://jsoup.org/
HTML parser
Note: the source code is embedded, do not upgrade

JUNG : The JUNG Framework Development Team, licence BSD
Version: 2.0.1 (25/01/2010)
http://jung.sourceforge.net/
For the modeling, analysis, and visualization of data that can be represented as a graph or network.

Log4J, Tika and Commons from apache.org
Version: 1.2.16

Miglayout : Miginfo.com, free BSD or GPL
Version: 4.0 (11/08/2011)
http://www.miglayout.com/
Initial developer: Mikael Grev
Layout manager.

SHEF : Bob Tantlinger, LGPL v2.1
Version: 2009-05-12
http://shef.sourceforge.net/
HTML editing framework and component library
SHEF uses originaly libraries:
- Swing Action Manager
- Novaworx SyntaxPane
SHEF is embeded in oStorybook

SLF4J : Copyright (c) 2004-2013 QOS.ch MIT license
Version: 1.7.21
http://www.slf4j.org
Logging API

novaworx : does not exist anymore, doesn't used anymore
Version: 0.0.7

RsyntaxTextArea: Robert Futrell, BSD 3-Clause license
Version: 3.0.6
RSyntaxTextArea is a customizable, syntax highlighting text component
Library: rsyntaxtextarea-3.0.6.jar

sam : Michael Bushe, Apache License 2.0
Version: 0.8.1 (??/??/2006)
does not exist anymore, doesn't used anymore
Swing Action Manager

tika (Apache Tika), Apache License 2.0
Version: 0.9.0 (16/02/2011)
Apache Tika is a toolkit for detecting and extracting metadata and
structured text content from various documents using existing parser
libraries.
Library: tika-core-0.9.jar

WoodSToX XML-processor (wstx) : Apache License 2.0
Version: 3.2.6 -> 4.4.1
(https://github.com/FasterXML/woodstox)
XML Parser
need for: JUNG

English synonyms extracted from Merriam-Webdster's dictionary
https://www.merriam-webster.com/

French synonyms extracted from CRISCO-DES
https://crisco4.unicaen.fr/des/